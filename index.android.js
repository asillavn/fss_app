import React from 'react';
import { AppRegistry} from 'react-native';
import AppNavigator from './src/appNavigator';

AppRegistry.registerComponent('RN_Project_test', () => AppNavigator);
