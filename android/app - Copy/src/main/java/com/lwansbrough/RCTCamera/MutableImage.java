package com.lwansbrough.RCTCamera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.ExifInterface;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.facebook.react.bridge.ReadableMap;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;

//import com.tzutalin.dlib.Constants;
//import com.tzutalin.dlib.FaceDet;
//import com.tzutalin.dlib.VisionDetRet;

import android.media.FaceDetector;
import android.media.FaceDetector.Face;
//import android.graphics.PointF;

import com.rn_project_test.MainApplication;

public class MutableImage {
    private static final String TAG = "RNCamera";

    private final byte[] originalImageData;
    private Bitmap currentRepresentation;
    private Metadata originalImageMetaData;
    private boolean hasBeenReoriented = false;
    private FaceDetector mFaceDetector;
    //private List<VisionDetRet> mFacesList;

    public MutableImage(byte[] originalImageData) {
        this.originalImageData = originalImageData;
        this.currentRepresentation = toBitmap(originalImageData);

        if (currentRepresentation.getWidth() > currentRepresentation.getHeight() && Build.VERSION.SDK_INT != Build.VERSION_CODES.KITKAT){
            final Matrix bitmapMatrix = new Matrix();
            bitmapMatrix.postRotate(90);

            this.currentRepresentation = Bitmap.createBitmap(
                    currentRepresentation,
                    0,
                    0,
                    currentRepresentation.getWidth(),
                    currentRepresentation.getHeight(),
                    bitmapMatrix,
                    false
            );

            //this.hasBeenReoriented = true;
        }

        this.mFaceDetector = new FaceDetector(720, 1280, 20);
    }

    public void mirrorImage() throws ImageMutationFailedException {
        Matrix m = new Matrix();

        m.preScale(-1, 1);

        Bitmap bitmap = Bitmap.createBitmap(
                currentRepresentation,
                0,
                0,
                currentRepresentation.getWidth(),
                currentRepresentation.getHeight(),
                m,
                false
        );

        if (bitmap == null)
            throw new ImageMutationFailedException("failed to mirror");

        this.currentRepresentation = bitmap;
    }

    public void fixOrientation() throws ImageMutationFailedException {
        try {
            Metadata metadata = originalImageMetaData();

            ExifIFD0Directory exifIFD0Directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            if (exifIFD0Directory == null) {
                return;
            } else if (exifIFD0Directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
                int exifOrientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
                if(exifOrientation != 1) {
                    rotate(exifOrientation);
                    exifIFD0Directory.setInt(ExifIFD0Directory.TAG_ORIENTATION, 1);
                }
            }
        } catch (ImageProcessingException | IOException | MetadataException e) {
            throw new ImageMutationFailedException("failed to fix orientation", e);
        }
    }

    //see http://www.impulseadventure.com/photo/exif-orientation.html
    private void rotate(int exifOrientation) throws ImageMutationFailedException {
        final Matrix bitmapMatrix = new Matrix();
        switch (exifOrientation) {
            case 1:
                return;//no rotation required
            case 2:
                bitmapMatrix.postScale(-1, 1);
                break;
            case 3:
                bitmapMatrix.postRotate(180);
                break;
            case 4:
                bitmapMatrix.postRotate(180);
                bitmapMatrix.postScale(-1, 1);
                break;
            case 5:
                bitmapMatrix.postRotate(90);
                bitmapMatrix.postScale(-1, 1);
                break;
            case 6:
                bitmapMatrix.postRotate(90);
                break;
            case 7:
                bitmapMatrix.postRotate(270);
                bitmapMatrix.postScale(-1, 1);
                break;
            case 8:
                bitmapMatrix.postRotate(270);
                break;
            default:
                break;
        }

        Bitmap transformedBitmap = Bitmap.createBitmap(
                currentRepresentation,
                0,
                0,
                currentRepresentation.getWidth(),
                currentRepresentation.getHeight(),
                bitmapMatrix,
                false
        );

        if (transformedBitmap == null)
            throw new ImageMutationFailedException("failed to rotate");

        this.currentRepresentation = transformedBitmap;
        this.hasBeenReoriented = true;
    }

    private Bitmap toBitmap(byte[] data) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            Bitmap photo = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return photo;
        } catch (IOException e) {
            throw new IllegalStateException("Will not happen", e);
        }
    }

    public String toBase64(int jpegQualityPercent) {
        return Base64.encodeToString(toJpeg(currentRepresentation, jpegQualityPercent), Base64.DEFAULT);
    }

    public void writeDataToFile(File file, ReadableMap options, int jpegQualityPercent) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);

        fos.write(toJpeg(currentRepresentation, jpegQualityPercent));
        fos.close();

        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());

            // copy original exif data to the output exif...
            // unfortunately, this Android ExifInterface class doesn't understand all the tags so we lose some
            for (Directory directory : originalImageMetaData().getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    int tagType = tag.getTagType();
                    Object object = directory.getObject(tagType);
                    exif.setAttribute(tag.getTagName(), object.toString());
                }
            }

            writeLocationExifData(options, exif);

            if(hasBeenReoriented)
                rewriteOrientation(exif);

            exif.saveAttributes();
        } catch (ImageProcessingException  | IOException e) {
            Log.e(TAG, "failed to save exif data", e);
        }
    }

    private void rewriteOrientation(ExifInterface exif) {
        exif.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(ExifInterface.ORIENTATION_NORMAL));
    }

    private void writeLocationExifData(ReadableMap options, ExifInterface exif) {
        if(!options.hasKey("metadata"))
            return;

        ReadableMap metadata = options.getMap("metadata");
        if (!metadata.hasKey("location"))
            return;

        ReadableMap location = metadata.getMap("location");
        if(!location.hasKey("coords"))
            return;

        try {
            ReadableMap coords = location.getMap("coords");
            double latitude = coords.getDouble("latitude");
            double longitude = coords.getDouble("longitude");

            GPS.writeExifData(latitude, longitude, exif);
        } catch (IOException e) {
            Log.e(TAG, "Couldn't write location data", e);
        }
    }

    private Metadata originalImageMetaData() throws ImageProcessingException, IOException {
        if(this.originalImageMetaData == null) {//this is expensive, don't do it more than once
            originalImageMetaData = ImageMetadataReader.readMetadata(
                    new BufferedInputStream(new ByteArrayInputStream(originalImageData)),
                    originalImageData.length
            );
        }
        return originalImageMetaData;
    }

    private byte[] toJpeg(Bitmap bitmap, int quality) throws OutOfMemoryError {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        // LeNT add process start
        //List<VisionDetRet> faceList = RCTCameraModule.mFaceDet.detect(bitmap);
        /*RCTCameraModule.writeLog("Draw heart [start]");
        Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        if(mFacesList != null && mFacesList.size() != 0){
            int[] faces = new int[mFacesList.size()*4];
            int i = 0;
            for (VisionDetRet each : mFacesList) {
                faces[i]=each.getLeft();
                faces[i+1]=each.getTop();
                faces[i+2]=each.getRight()-each.getLeft();//width
                faces[i+3]=each.getBottom()-each.getTop();//height
                i+=4;
            }
            Canvas canvas = new Canvas(mutableBitmap);
            Bitmap heart = MainApplication.getBitmapFromAsset("images/heart.png");
            for (int j = 0; j < mFacesList.size(); j++) {
                Bitmap scaledHeart = resizeBitmap(heart, (int) (faces[4 * j + 2]), (int) (faces[4 * j + 3]));
                canvas.drawBitmap(scaledHeart, (int) (faces[4 * j]), (int) (faces[4 * j + 1]), null);
            }
        }
        RCTCameraModule.writeLog("Draw heart [end]");*/
        // LeNT add process end
        int w = this.currentRepresentation.getWidth();
        int h = this.currentRepresentation.getHeight();
        if (h != 1280 || w != 720)
            mFaceDetector = new FaceDetector(w, h, 20);

        Bitmap facesBitmap = this.currentRepresentation.copy(Bitmap.Config.RGB_565, true);

        Face[] faces = new Face[20];
        RCTCameraModule.writeLog("Face detector [start]");
        int count = mFaceDetector.findFaces(facesBitmap, faces);
        RCTCameraModule.writeLog("Face detector [end]");
        facesBitmap.recycle();

        //this.mFacesList = new ArrayList<VisionDetRet>();
        RCTCameraModule.writeLog("Draw heart [start]");
        this.currentRepresentation = this.currentRepresentation.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvas = new Canvas(this.currentRepresentation);
        Bitmap heart = MainApplication.getBitmapFromAsset("images/heart.png");

        PointF midPoint = new PointF();
        if (count > 0){
            Bitmap scaledHeart;
            for (int i = 0; i < count; i++){
                Face face = faces[i];
                face.getMidPoint(midPoint);

                int top = (int)midPoint.y;
                int left = (int)midPoint.x;
                int width = (int)(face.eyesDistance() * 2);

                scaledHeart = resizeBitmap(heart, width, width);

                width = width / 2;
                top -= width * 2 / 3;
                left -= width;

                canvas.drawBitmap(scaledHeart, left, top, null);

                scaledHeart.recycle();
            }
        }
        RCTCameraModule.writeLog("Draw heart [end]");

        //mutableBitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
        this.currentRepresentation.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);

        try {
            return outputStream.toByteArray();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                Log.e(TAG, "problem compressing jpeg", e);
            }
        }
    }

    public static class ImageMutationFailedException extends Exception {
        public ImageMutationFailedException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public ImageMutationFailedException(String detailMessage) {
            super(detailMessage);
        }
    }

    private static class GPS {
        public static void writeExifData(double latitude, double longitude, ExifInterface exif) throws IOException {
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, toDegreeMinuteSecods(latitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, latitudeRef(latitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, toDegreeMinuteSecods(longitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, longitudeRef(longitude));
        }

        private static String latitudeRef(double latitude) {
            return latitude < 0.0d ? "S" : "N";
        }

        private static String longitudeRef(double longitude) {
            return longitude < 0.0d ? "W" : "E";
        }

        private static String toDegreeMinuteSecods(double latitude) {
            latitude = Math.abs(latitude);
            int degree = (int) latitude;
            latitude *= 60;
            latitude -= (degree * 60.0d);
            int minute = (int) latitude;
            latitude *= 60;
            latitude -= (minute * 60.0d);
            int second = (int) (latitude * 1000.0d);

            StringBuffer sb = new StringBuffer();
            sb.append(degree);
            sb.append("/1,");
            sb.append(minute);
            sb.append("/1,");
            sb.append(second);
            sb.append("/1000,");
            return sb.toString();
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap,int newWidth,int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }
}
