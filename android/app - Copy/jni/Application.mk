# APP_ABI := all
# APP_ABI      := armeabi armeabi-v7a armeabi-v7a-hard arm64-v8a mips mips64 x86 x86_64
APP_ABI      := armeabi armeabi-v7a mips x86
# APP_ABI      := armeabi-v7a

APP_CPPFLAGS := -std=c++11
#LOCAL_CFLAGS := -std=c++11
APP_STL := gnustl_shared
APP_CPPFLAGS := -frtti -fexceptions -latomic
# APP_STL += gnustl_shared
