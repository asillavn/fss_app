/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cstdint>
#include <cstdio>
#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <android/log.h>
#include <time.h>

#include <fstream>

using std::ofstream;

#include <iostream>
#include <fstream>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <android/bitmap.h>

#define HELLOAPP "com.example.spudlord.helloapp"

using namespace cv;
using namespace std;

string type2str(int type) {
    string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch (depth) {
        case CV_8U:
            r = "8U";
            break;
        case CV_8S:
            r = "8S";
            break;
        case CV_16U:
            r = "16U";
            break;
        case CV_16S:
            r = "16S";
            break;
        case CV_32S:
            r = "32S";
            break;
        case CV_32F:
            r = "32F";
            break;
        case CV_64F:
            r = "64F";
            break;
        default:
            r = "User";
            break;
    }

    r += "C";
    r += (chans + '0');

    return r;
}

Mat convertBitmapToMat(JNIEnv *env, jobject bitmap, bool needUnPremultiplyAlpha) {
    AndroidBitmapInfo info;
    void *pixels = 0;
    AndroidBitmap_getInfo(env, bitmap, &info);
    AndroidBitmap_lockPixels(env, bitmap, &pixels);
    Mat dst;
    dst.create(info.height, info.width, CV_8UC4);
    if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        Mat tmp(info.height, info.width, CV_8UC4, pixels); // CV_8UC4
        cvtColor(tmp, tmp, COLOR_RGB2RGBA);
        if (needUnPremultiplyAlpha) cvtColor(tmp, dst, COLOR_mRGBA2RGBA);
        else tmp.copyTo(dst);
    } else {
        Mat tmp(info.height, info.width, CV_8UC2, pixels);
        cvtColor(tmp, dst, COLOR_BGR5652RGBA);
    }
    AndroidBitmap_unlockPixels(env, bitmap);
    return dst;
}

jobject createBitmap(JNIEnv *env, int w, int h, const char *pixelFormat) {
    jclass java_bitmap_class = (jclass) env->FindClass("android/graphics/Bitmap");
    jmethodID mid = env->GetStaticMethodID(java_bitmap_class, "createBitmap",
                                           "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    jclass bcfg_class = env->FindClass("android/graphics/Bitmap$Config");
    jfieldID formatObj = env->GetStaticFieldID(bcfg_class, pixelFormat,
                                               "Landroid/graphics/Bitmap$Config;");
    jobject java_bitmap_config = env->GetStaticObjectField(bcfg_class, formatObj);
    jobject java_bitmap = env->CallStaticObjectMethod(java_bitmap_class, mid, w, h,
                                                      java_bitmap_config);
    return java_bitmap;
}

jobject convertMatToBitmap(JNIEnv *env, Mat src, bool needPremultiplyAlpha) {
    jobject bitmap = createBitmap(env, src.size().width, src.size().height, "RGB_565");
    AndroidBitmapInfo info;
    void *pixels = 0;
    AndroidBitmap_getInfo(env, bitmap, &info);
    AndroidBitmap_lockPixels(env, bitmap, &pixels);
    if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        Mat tmp(info.height, info.width, CV_8UC4, pixels);
        if (src.type() == CV_8UC1) {
            cvtColor(src, tmp, COLOR_GRAY2RGBA);
        } else if (src.type() == CV_8UC3) {
            cvtColor(src, tmp, COLOR_RGB2RGBA);
        } else if (src.type() == CV_8UC4) {
            if (needPremultiplyAlpha) cvtColor(src, tmp, COLOR_RGBA2mRGBA);
            else src.copyTo(tmp);
        }
    } else {
        // info.format == ANDROID_BITMAP_FORMAT_RGB_565
        Mat tmp(info.height, info.width, CV_8UC2, pixels);
        if (src.type() == CV_8UC1) {
            cvtColor(src, tmp, COLOR_GRAY2BGR565);
        } else if (src.type() == CV_8UC3) {
            cvtColor(src, tmp, COLOR_RGB2BGR565);
        } else if (src.type() == CV_8UC4) {
            cvtColor(src, tmp, COLOR_RGBA2BGR565);
        }
    }
    AndroidBitmap_unlockPixels(env, bitmap);
    return bitmap;
}


Mat createGrayScale(Mat src) {
    if (src.channels() == 3 || src.channels() == 4) {
        Mat gs_rgb(src.size(), CV_8UC1);
        cvtColor(src, gs_rgb, CV_RGB2GRAY);
        return gs_rgb;
    }
    return src;
}

Mat rotateImage(Mat src, int angle) {
    int len = src.cols > src.rows ? src.cols : src.rows;
    Point2f src_center(len / 2.0F, len / 2.0F);
    Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
    Mat dst;
    warpAffine(src, dst, rot_mat, Size(src.size().height, src.size().width));
    return dst;
}

static double now_ms(void) {
    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
}

CascadeClassifier *face_cascade = 0;

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
extern "C" JNIEXPORT jintArray JNICALL
Java_com_nara_opencv_OpenCV_detectFaces(JNIEnv *env,
                                        jobject thiz, jbyteArray rawBytes,
                                        jint jWidth, jint jHeight,
                                        jstring jXmlPath, jint isTestDirExisted,
                                        jstring jTestDirectory) {
/*init_convertion_rgb565();

AndroidBitmapInfo bi = {0};
    uint8_t *pixelBuf;
    uint8_t a, r, g, b;

    AndroidBitmap_getInfo(env, bitmap, &bi);
__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale azzzzzzzzzzz");
    std::vector<uint8_t> data22;
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Gbi.format=%d", bi.format);
    //assert(ANDROID_BITMAP_FORMAT_RGBA_8888 == bi.format);
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale hgggggggggggggg");
    AndroidBitmap_lockPixels(env, bitmap, (void **)&pixelBuf);

char* sourcee = (char*)pixelBuf;*/


/*uint8_t* sourcee = new uint8_t[bi.width * bi.height];
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale qaaazzzzz");
    convert_from2_rgb565(sourcee, pixelBuf, bi.width * bi.height);*/

    /*for (int y = 0; y < bi.height; y++) {
        for (int x = 0; x < bi.width; x++) {
            r = pixelBuf[y * bi.stride + x * 4 + 0];
            g = pixelBuf[y * bi.stride + x * 4 + 1];
            b = pixelBuf[y * bi.stride + x * 4 + 2];
            a = pixelBuf[y * bi.stride + x * 4 + 3];
            data22.push_back(a << 24 | r << 16 | g << 8 | b);
        }
    }*/
    //AndroidBitmap_unlockPixels(env, bitmap);
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale sizze=%d", data22.size());

/*AndroidBitmapInfo androidBitmapInfo ;
    void* pixels;
    AndroidBitmap_getInfo(env, bitmap, &androidBitmapInfo);
    AndroidBitmap_lockPixels(env, bitmap, &pixels);
    unsigned char* pixelsChar = (unsigned char*) pixels;
__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale ccccccccc=%d   %d", androidBitmapInfo.width, androidBitmapInfo.height);
int lsizes = strlen((const char*)pixelsChar);
__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale zzzzzzzzzzz=%d", lsizes);
    std::ofstream fs("/storage/sdcard0/TestDir/check.bmp", std::ios::out | std::ios::binary | std::ios::app);
            fs.write((const char*)pixelsChar, bitsize);
            fs.close();*/
//__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale mcmmmmmmm");


/*unsigned char * isCopy;
    jbyte* cData = env->GetByteArrayElements(data, isCopy);
    jsize dataLen = env->GetArrayLength(data);*/

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);

/*unsigned char * isCopy;
jbyte* cData = env->GetByteArrayElements(data, isCopy);
char * imageSource = (char *)cData;*/



    /*unsigned char * isCopy;
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale allllllllllllllll");
  jbyte* cData = env->GetByteArrayElements(data, isCopy);
  __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale aqqqqqqqqqqqqqqqqqqqqq");
  jsize dataLen = env->GetArrayLength(data);
  __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale datalen=%d", (int)dataLen);
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale bitsize=%d", (int)bitsize);
  char * imageSource = (char *)cData;
  vector<char> imageSourceV;
  __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale sickkkkkkk");
  for (int i = 0; i < dataLen; i++) {
    imageSourceV.push_back(imageSource[i]);
  }*/
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale ohhh sitttttt");
    //read_jpeg_file("/storage/sdcard0/TestDir/Bua02.jpg");

    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale width=%d height=%d", width, height);

    //convert jbyteArray to vector<char>
    /*unsigned char * isCopy;
    //jbyte* jbae = env->GetByteArrayElements(data, isCopy);
    jsize len = env->GetArrayLength(data);
    char * imageSource = (char *)cData;*/


    /*int lsize = strlen((const char*)pixelsChar);
    vector<char> imageSourceV;
          for (int i = 0; i < bitsize; i++) {
            imageSourceV.push_back(pixelsChar[i]);
          }*/

    //char * imageSource = (char *)cData;
    //vector<char> imageSourceV(sourcee, sourcee + (int)bitsize);

    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale width====");

//int zbitsize = (int)bitsize;
    //Mat rawData  =  Mat( 1, zbitsize, CV_8UC1, pixelsChar );
    //Mat mat  =  imdecode( imageSourceV, CV_LOAD_IMAGE_COLOR);

    //Mat mat = imdecode(Mat(imageSourceV), 1);
    /*Mat bgra(androidBitmapInfo.height, androidBitmapInfo.width, CV_8UC4, pixelsChar);
    Mat mat;
    cvtColor(bgra, mat, COLOR_RGBA2BGRA);

    imwrite("/storage/sdcard0/TestDir/comparetest22.png", mat, compression_params);*/
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale zssssss");

    //Mat mat = Mat(height, width, CV_32FC4, sourcee);
    //Mat mat = imdecode(imageSourceV, CV_LOAD_IMAGE_COLOR);

    /*Mat mat;
    mat.create(bi.height, bi.width, CV_8UC4);
    Mat tmp(bi.height, bi.width, CV_8UC2, pixelBuf);
    cvtColor(tmp, mat, COLOR_BGR5652RGBA);*/

    int width = (int) jWidth;
    int height = (int) jHeight;
    //uint8_t *srcLumaPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(srcBuffer));
    jbyte *byteData = env->GetByteArrayElements(rawBytes, 0);
    Mat mYuv(height + height / 2, width, CV_8UC1, (unsigned char *) byteData);
    Mat gs_rgb(height, width, CV_8UC1);
    cvtColor(mYuv, gs_rgb, CV_YUV2GRAY_NV21);

    env->ReleaseByteArrayElements(rawBytes, byteData, 0);

    //Mat mat = convertBitmapToMat(env, bitmap, false);



    //Mat mat = Mat(imageSourceV, CV_32FC1);
    //Mat mat = imdecode(Mat(imageSourceV, CV_64FC4), CV_LOAD_IMAGE_COLOR);

    /*Mat mat22 = imread("/storage/emulated/0/TestDir/Bua02.jpg", IMREAD_UNCHANGED);
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "mat22 depth=%d", mat22.depth());
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "mat22 channel=%d", mat22.channels());
    int xxx = mat22.type();
        __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, type2str(xxx).c_str());
__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "mat22 w=%d h=%d", mat22.rows, mat22.cols);

imwrite("/storage/emulated/0/TestDir/Bua022.jpg", mat22, compression_params);*/

    //std::ifstream file("/storage/emulated/0/TestDir/Bua02.jpg");
    // std::vector<char> data2;


    //std::vector<char> data2(imageSource, imageSource + bitsize);

    //file >> std::noskipws;
    //std::copy(std::istream_iterator<char>(file), std::istream_iterator<char>(), std::back_inserter(data2));

    //Mat mat = imdecode(Mat(data22), 1);

    //Mat mat = imdecode(Mat(imageSourceV), 1);


    /*if (mat.data == NULL) {
        __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale shitttttttttttttt");
    }
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale depth=%d", mat.depth());
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale channel=%d", mat.channels());
        __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale type=%d", mat.type());
__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale w=%d h=%d", mat.rows, mat.cols);*/



    //Mat mat;
    //cvtColor( mat22,mat, CV_BGR2BGRA );
    //mat22.convertTo(mat, CV_8UC4);


//__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale aaaaaaaaaaaaaaaa");

    //imwrite("/storage/sdcard0/TestDir/comparetest2.png", mat, compression_params);
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale qqqqqqqqqqqqqqqqqq");

    //Mat gs_rgb(mat.size(), CV_8UC1);
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale hhhhhhhhhhhhhhhhhhh");
    //cvtColor(mat, gs_rgb, CV_RGB2GRAY);

    /*Mat gs_rgb(mat);
    if (mat.channels() == 3 || mat.channels() == 4) {
        Mat gs_rgb(mat.size(), CV_8UC1);
        cvtColor(mat, gs_rgb, CV_RGB2GRAY);
        tmp.copyTo(dst);
    } else {
        gs_rgb(mat);
    }*/


    //Mat gs_rgb = createGrayScale(mat);
    //gs_rgb = rotateImage(gs_rgb, 90);

    const char *xmlPath = env->GetStringUTFChars(jXmlPath, 0);
    vector<Rect> faces;
    //CascadeClassifier face_cascade;
    if (!face_cascade) {
        //__android_log_print(ANDROID_LOG_ERROR, HELLOAPP, "Grayscale sheeeeeeeeeeeeet");
        face_cascade = new CascadeClassifier();
        face_cascade->load(xmlPath);
    }

    face_cascade->detectMultiScale(gs_rgb, faces, 2, 1,
                                   CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE,
                                   Size(30, 30), Size(900, 900));

    //__android_log_print(ANDROID_LOG_ERROR, HELLOAPP, "Grayscale yessssss 1");
    jint faceData[faces.size() * 4];
    //faceData[0] = 112;
    //__android_log_print(ANDROID_LOG_ERROR, HELLOAPP, "Grayscale yessssss 2");
    int testDirExisted = (int) isTestDirExisted;
    if (faces.size() > 0) {
        int index;
        Rect face;
        for (index = 0; index < faces.size(); index++) {
            face = faces[index];
            if (testDirExisted == 1) {
                rectangle(gs_rgb, face, Scalar(255, 0, 0), 3);
            }
            faceData[index * 4] = face.x;
            faceData[index * 4 + 1] = face.y;
            faceData[index * 4 + 2] = face.width;
            faceData[index * 4 + 3] = face.height;
        }
    }
    env->ReleaseStringUTFChars(jXmlPath, xmlPath);

    jintArray resultData;
    resultData = env->NewIntArray(faces.size() * 4);
    env->SetIntArrayRegion(resultData, 0, faces.size() * 4, faceData);

    if (faces.size() > 0 && testDirExisted == 1) {
        const char *testDirectory = env->GetStringUTFChars(jTestDirectory, 0);
        double noww = now_ms();
        char dir[1000];
        sprintf(dir, "%s/test_%f.png", testDirectory, noww);
        imwrite(dir, gs_rgb, compression_params);
    }

    //int size = (int) gs_rgb.total() * gs_rgb.channels();

    /*int rows = gs_rgb.rows;
    int cols = gs_rgb.cols;
    int num_el = rows*cols;
    int size = num_el*gs_rgb.elemSize1();

    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale c size=%d", size);

    std::vector<unsigned char> buff;//buffer for coding
    std::vector<int> param(2);
    param[0] = IMWRITE_JPEG_QUALITY;
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale where shit beee");
    param[1] = 80;//default(95) 0-100
    imencode(".jpg", gs_rgb, buff, param);
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale shit pass");
    size = buff.size() * sizeof(unsigned char);



    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale where shit hap");

    unsigned char* dataMat = reinterpret_cast<unsigned char*>(buff.data());
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale shit more");
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale where shit size == %d", size);
    jbyteArray array = env->NewByteArray (size);
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale shit yes");
    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale where shit sick");
    env->SetByteArrayRegion (array, 0, size, reinterpret_cast<jbyte*>(dataMat));
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale shit yehhh");*/

    //jobject bitmapp = convertMatToBitmap(env, gs_rgb, false);

    /*__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale where shit up");

    jbyte* prt;
    jbyteArray array2 = env->NewByteArray (10);
    env->SetByteArrayRegion (array2, 0, 10, reinterpret_cast<jbyte*>(dataMat));
    prt = env->GetByteArrayElements(array2, 0);
    prt[10] = 0;
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale dest=%s", prt);*/

    //env->ReleaseByteArrayElements(data, cData, JNI_ABORT);

    /*imwrite("/storage/sdcard0/TestDir/comparetest.png", gs_rgb, compression_params);

    Mat mat2 = Mat(height, width, CV_8UC1, dataMat).clone();
    imwrite("/storage/sdcard0/TestDir/comparetest1.png", mat2, compression_params);*/

    int size22;
    /*char * memblock;
    ifstream file ("/storage/sdcard0/TestDir/comparetest1.png", ios::in|ios::binary|ios::ate);
      if (file.is_open())
      {
        size22 = file.tellg();
            memblock = new char [size22];
            file.seekg (0, ios::beg);
            file.read (memblock, size22);
            file.close();

            array = env->NewByteArray (size22);
                env->SetByteArrayRegion (array, 0, size22, reinterpret_cast<jbyte*>(memblock));

        delete[] memblock;
      }*/


    //__android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale by OpenCV");

    /*int len = strlen(str);
    char out[len + 1];
    for (int i = 0; i < len; i++) {
        out[i] = str[i];
    }
    out[len] = '\0';
    char integer_string[32];
        int integer = return_thats(6);
        sprintf(integer_string, "%d", len);
        strcat(str, integer_string);*/

    return resultData;
}


extern "C" JNIEXPORT jintArray JNICALL
Java_com_nara_opencv_OpenCV_captureFaces(JNIEnv *env,
                                         jobject thiz, jobject bitmap,
                                         jstring jXmlPath) {
    Mat mat = convertBitmapToMat(env, bitmap, false);
    Mat gs_rgb = createGrayScale(mat);

    const char *xmlPath = env->GetStringUTFChars(jXmlPath, 0);
    vector<Rect> faces;
    if (!face_cascade) {
        face_cascade = new CascadeClassifier();
        face_cascade->load(xmlPath);
    }

    face_cascade->detectMultiScale(gs_rgb, faces, 2, 1,
                                   CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE,
                                   Size(30, 30), Size(900, 900));
    jint faceData[faces.size() * 4];
    __android_log_print(ANDROID_LOG_DEBUG, HELLOAPP, "Grayscale captureFaces faces=%d", faces.size());
    if (faces.size() > 0) {
        int index;
        Rect face;
        for (index = 0; index < faces.size(); index++) {
            face = faces[index];
            //rectangle(gs_rgb, face, Scalar(255, 0, 0), 3);
            faceData[index * 4] = face.x;
            faceData[index * 4 + 1] = face.y;
            faceData[index * 4 + 2] = face.width;
            faceData[index * 4 + 3] = face.height;
        }
    }
    env->ReleaseStringUTFChars(jXmlPath, xmlPath);

    /*vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);
    imwrite("/storage/sdcard0/TestDir3/_____FILE.png", gs_rgb, compression_params);*/

    jintArray resultData;
    resultData = env->NewIntArray(faces.size() * 4);
    env->SetIntArrayRegion(resultData, 0, faces.size() * 4, faceData);

    return resultData;
}

extern "C" JNIEXPORT jintArray JNICALL
Java_com_nara_opencv_OpenCV_captureJpegFaces(JNIEnv *env,
                                             jobject thiz, jbyteArray rawBytes, jint rawSize,
                                             jstring jXmlPath, jint isTestDirExisted,
                                             jstring jTestDirectory) {
    jbyte *byteData = env->GetByteArrayElements(rawBytes, 0);
    vector<char> charVector;
    for (int i = 0; i < rawSize; i++) {
        charVector.push_back(byteData[i]);
    }
    Mat gs_rgb = imdecode(charVector, IMREAD_GRAYSCALE);

    //Mat mat = convertBitmapToMat(env, bitmap, false);
    //Mat gs_rgb = createGrayScale(mat);

    const char *xmlPath = env->GetStringUTFChars(jXmlPath, 0);
    vector<Rect> faces;
    if (!face_cascade) {
        face_cascade = new CascadeClassifier();
        face_cascade->load(xmlPath);
    }

    int testDirExisted = (int) isTestDirExisted;

    face_cascade->detectMultiScale(gs_rgb, faces, 2, 1,
                                   CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE,
                                   Size(30, 30), Size(900, 900));
    jint faceData[faces.size() * 4];
    __android_log_print(ANDROID_LOG_ERROR, HELLOAPP, "Grayscalesssssss faces=%d", faces.size());
    if (faces.size() > 0) {
        int index;
        Rect face;
        for (index = 0; index < faces.size(); index++) {
            face = faces[index];
            if (testDirExisted == 1)
            rectangle(gs_rgb, face, Scalar(255, 0, 0), 3);
            faceData[index * 4] = face.x;
            faceData[index * 4 + 1] = face.y;
            faceData[index * 4 + 2] = face.width;
            faceData[index * 4 + 3] = face.height;
        }
    }
    env->ReleaseStringUTFChars(jXmlPath, xmlPath);

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);

    if (faces.size() > 0 && testDirExisted == 1) {
        const char *testDirectory = env->GetStringUTFChars(jTestDirectory, 0);
        double noww = now_ms();
        char dir[1000];
        sprintf(dir, "%s/take_%f.png", testDirectory, noww);
        imwrite(dir, gs_rgb, compression_params);
    }
    //imwrite("/storage/sdcard0/TestDir/_____FILE.png", gs_rgb, compression_params);

    jintArray resultData;
    resultData = env->NewIntArray(faces.size() * 4);
    env->SetIntArrayRegion(resultData, 0, faces.size() * 4, faceData);

    env->ReleaseByteArrayElements(rawBytes, byteData, 0);

    return resultData;
}

