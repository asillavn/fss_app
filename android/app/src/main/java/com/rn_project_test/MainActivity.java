package com.rn_project_test;

import android.content.Context;
import android.os.Bundle;

import com.facebook.react.ReactActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
//import com.tzutalin.dlib.FaceDet;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "RN_Project_test";
    }

    //FaceDet mFaceDet;

    private static String learningDataPath;

    public static String getLearningDataPath() {
        return learningDataPath;
    }

    public void extractLearnData() {
        final InputStream is;
        FileOutputStream os;
        try {
            is = getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt.xml");
            learningDataPath = mCascadeFile.getAbsolutePath();
            os = new FileOutputStream(mCascadeFile);
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();
        } catch (IOException e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
