package com.rn_project_test;

import android.app.Application;
import android.content.Context;

import com.RNFetchBlob.RNFetchBlobPackage;
import com.facebook.react.ReactApplication;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.rnfs.RNFSPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNViewShotPackage(),
            new RNFSPackage(),
              new RCTCameraPackage(),
              new RNFetchBlobPackage(),
              new RNDeviceInfo(),
              new AppPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());
    MainApplication.context = getApplicationContext();
    SoLoader.init(this, /* native exopackage */ false);
  }

  private static Context context;

  public static Context getAppContext() {
    return MainApplication.context;
  }

  private static String learningDataPath;

  public static String getLearningDataPath() {
    if (learningDataPath == null) {
      extractLearnData();
    }
    return learningDataPath;
  }

    public static Bitmap getBitmapFromAsset(String filePath) {
        AssetManager assetManager = getAppContext().getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
        }
        return bitmap;
    }

  public static void extractLearnData() {
    InputStream is = null;
    FileOutputStream os = null;
    try {
      is = getAppContext().getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
      File cascadeDir = getAppContext().getDir("cascade", Context.MODE_PRIVATE);
      File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt.xml");
      learningDataPath = mCascadeFile.getAbsolutePath();
      os = new FileOutputStream(mCascadeFile);
      byte[] buffer = new byte[4096];
      int bytesRead;
      while ((bytesRead = is.read(buffer)) != -1) {
        os.write(buffer, 0, bytesRead);
      }
    } catch (IOException e) {
    } finally {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
            }
        }
    }
  }
  
  private static int isTestPathExisted = -1;
  
  public static boolean isTestDirExisted() {
        if (isTestPathExisted == -1) {
            File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/TestDir/");
            isTestPathExisted = directory.exists() ? 1 : 0;
        }
        return isTestPathExisted == 1;
    }
    
    public static String getTestDirectory() {
        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/TestDir/");
        return directory.getAbsolutePath();
    }

    public static File getTestTempTakedImage() throws IOException {
        java.util.Date date = new java.util.Date();
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        String format = formatter.format(date);

        File outputDir = getAppContext().getCacheDir();
        return File.createTempFile("IMG_" + format, ".jpg", outputDir);

        //return new File(getTestDirectory() + "/IMG_" + format + ".jpg");
    }
}
