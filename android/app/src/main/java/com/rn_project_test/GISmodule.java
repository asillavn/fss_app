package com.rn_project_test;

/**
 * Created by TrungDV on 1/12/2018.
 */

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import android.location.LocationListener;

public class GISmodule extends ReactContextBaseJavaModule {
    private static Location _location = null;
    private static Location _lastLocation = null;
    private LocationManager _locationManager = null;

    public GISmodule(ReactApplicationContext reactContext) {
        super(reactContext);
        _locationManager = (LocationManager) this.getReactApplicationContext().getSystemService(this.getReactApplicationContext().LOCATION_SERVICE);
    }

    @Override
    public String getName() {
        return "GISmodule";
    }

    @ReactMethod
    public void getCurrentLocation(final Promise promise) {
        WritableMap response = new WritableNativeMap();
        if (!checkServiceEnabled()) {
            response.putBoolean("enabled", false);
        } else {
            Location currentLocation = getCurrentLocation();
            if (currentLocation != null) {
                response.putDouble("lat", currentLocation.getLatitude());
                response.putDouble("lng", currentLocation.getLongitude());
            }
        }

        promise.resolve(response);
    }

    private LocationListener _locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            _location = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private boolean checkGPSEnabled() {
        return _locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean checkNetworkEnabled() {
        return _locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean checkServiceEnabled() {
        // getting GPS status
        boolean isGPSEnabled = checkGPSEnabled();
        // getting network status
        boolean isNetworkEnabled = checkNetworkEnabled();

        return isGPSEnabled || isNetworkEnabled;
    }

    public Location getCurrentLocation() {
        if (_locationManager != null && ActivityCompat.checkSelfPermission(this.getCurrentActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getCurrentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (_location == _lastLocation) {
                try {
                    if (checkServiceEnabled()) {
                        // if GPS Enabled get lat/long using GPS Services
                        if (checkGPSEnabled()) {
                            _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, _locationListener);
//                            Log.d("GPS", "GPS Enabled");
                            if (_locationManager != null) {
                                _location = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                _lastLocation = _location;
                            }
                        }

                        if (checkNetworkEnabled() && _location == null) {
                            _locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, _locationListener);
//                        Log.d("Network", "Network Enabled");
                            if (_locationManager != null) {
                                _location = _locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                _lastLocation = _location;
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return _location;
        }

        return null;
    }
}
