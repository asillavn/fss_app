import {Dimensions} from 'react-native';

exports.title = 'GlobalConfig';
exports.limitSlideHome = 7;
exports.limitArticleHome = 20;
exports.priority = {priorityOne: 1, priorityTwo: 2};
exports.limitRelateArticle = 5;

/* ===============================
 Default Style Variables
 =============================== */
// Window Dimensions
var window = Dimensions.get('window');
exports.windowHeight = window.height;
exports.windowWidth = window.width;

// General Element Dimensions
var navbarHeight = 64;
exports.navbarHeight = navbarHeight;
exports.statusBarHeight = 20;
exports.iconSize = 30;

// Grid
exports.gridHalf = window.width * 0.5;
exports.gridThird = window.width * 0.333;
exports.gridTwoThirds = window.width * 0.666;
exports.gridQuarter = window.width * 0.25;
exports.gridThreeQuarters = window.width * 0.75;

// Fonts
exports.baseFont = 'Avenir';

// Colors
exports.primaryColor = "#FFF";
exports.VNExColor = "#9f224e";
