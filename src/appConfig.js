import React from 'react';
import
{
    Dimensions
} from 'react-native';

exports.title = 'GlobalConfig';
exports.limitSlideHome = 7;
exports.limitArticleHome = 20;
exports.priority = {priorityOne: 1, priorityTwo: 2};
exports.limitRelateArticle = 5;

/* ===============================
 Default Style Variables
 =============================== */
// Window Dimensions
var window = Dimensions.get('window');
exports.windowHeight = window.height;
exports.windowWidth = window.width;

// General Element Dimensions
var navbarHeight = 56;
exports.navbarHeight = navbarHeight;
exports.statusBarHeight = 20;
exports.iconSize = 30;

// Grid
exports.gridHalf = window.width * 0.5;
exports.gridThird = window.width * 0.333;
exports.gridTwoThirds = window.width * 0.666;
exports.gridQuarter = window.width * 0.25;
exports.gridThreeQuarters = window.width * 0.75;

// Fonts
exports.baseFont = 'Avenir';
exports.font = {
    myButton: {
        fontSize: 30
    },
    navButton: {
        fontSize: 25
    },
    default: {
        fontSize: 20
    }
}

// Colors
exports.primaryColor = "#7AB848";
exports.secondaryColor = "#ffc704";

// maximum request size
exports.request = {
    maxPackageSize: 4, // MB
    sizeof: (object) => {

        // initialise the list of objects and size
        var objects = [object];
        var size    = 0;

        // loop over the objects
        for (var index = 0; index < objects.length; index ++){

            // determine the type of the object
            switch (typeof objects[index]){

                // the object is a boolean
                case 'boolean': size += 4; break;

                // the object is a number
                case 'number': size += 8; break;

                // the object is a string
                case 'string': size += 2 * objects[index].length; break;

                // the object is a generic object
                case 'object':

                    // if the object is not an array, add the sizes of the keys
                    if (Object.prototype.toString.call(objects[index]) != '[object Array]'){
                        for (var key in objects[index]) size += 2 * key.length;
                    }

                    // loop over the keys
                    for (var key in objects[index]){

                        // determine whether the value has already been processed
                        var processed = false;
                        for (var search = 0; search < objects.length; search ++){
                            if (objects[search] === objects[index][key]){
                                processed = true;
                                break;
                            }
                        }

                        // queue the value to be processed if appropriate
                        if (!processed) objects.push(objects[index][key]);

                    }

            }

        }

        // return the calculated size
        return size;

    }
}
var mode = {
    family: 0,
    supporter: 1
}
exports.mode = mode;

var language = {
    current: 1,
    data: [
        {
            back: 'Back',
            caution: 'Caution',
            yes: 'Yes',
            no: 'No',
            ok: 'Ok',
            start_screen_familyMode: 'Family',
            start_screen_supporterMode: 'Supporter',
            caution_screen_disclaimer: 'Disclaimer',
            caution_screen_guideline: 'Guidelines',
            edit_screen_question: 'Is image include the face?',
            information_screen_familyID: 'Family ID',
            information_screen_email: 'Email',
            information_screen_sex: 'Sex',
            information_screen_age: 'Age',
            information_screen_upclothes: 'Up clothes',
            information_screen_lowclothes: 'Low clothes',
            information_screen_fullclothes: 'Full clothes',
            information_screen_hat: 'Hat',
            information_screen_bag: 'Bag',
            information_screen_shoe: 'Shoe',
            information_screen_unknown: 'Unknown',
            information_screen_color: 'Color',
            information_screen_send: 'Send',
            information_screen_baseLineData: {
                sex: [
                    'Unknown',
                    'Male',
                    'Female'
                ],
                age: [
                    'Unknown',
                    '10s',
                    '20s',
                    '30s',
                    '40s',
                    '50s',
                    '60s',
                    '70s or late'
                ],
                upClothes: [
                    'Unknown',
                    'jacket-coat',
                    'suit-vest',
                    'sweater',
                    'shirt-blouse',
                    't-shirt'
                ],
                lowClothes: [
                    'Unknown',
                    'jean',
                    'pants',
                    'short',
                    'skirt'
                ],
                fullClothes: [
                    'Unknown',
                    'dress',
                    'jumpsuit'
                ],
                hat: [
                    'Unknown',
                    'Yes',
                    'No'
                ],
                bag: [
                    'Unknown',
                    'Yes',
                    'No'
                ],
                shoe: [
                    'Unknown',
                    'shoe',
                    'boot',
                    'no'
                ],
                color: [
                    'Unknown',
                    'red',
                    'orange',
                    'yellow',
                    'purple',
                    'white',
                    'green',
                    'blue',
                    'black',
                    'gray'
                ]
            },
            camera_screen_saving: 'Saving',
            information_screen_sending: 'Sending',
            information_screen_can_not_get_location: "Can't get current position. Please enable GPS on this device.",
            message_box_send_successfully: (mode)=>{
                return mode === mode.supporter ? '画像を送信しました。 \nご協力ありがとうございます！' : '画像を送信しました。'
            },
            message_box_send_failed: (err)=>{
                return `Because [${err}]. Image was sent failed.\n\nDo you want resend?\n`
            },
            message_box_fillout: (arr)=>{
                return `Please fill out ${arr.join(" and ")}`
            },
            email_is_not_valid: "Email isn't valid",
            caution_screen_guidelineContent: 'Please help us by sending information of troubled-people as below:\nStep 1:\nTake a picture of troubled-people with this application.\nStep 2:\nBlur all private informations like face, name-place...\nStep 3:\nAdd information about year old, clothes, shapes and other information eg hat, identity character...\nStep 4:\nSend to our server by click on Send button.',
            caution_screen_disclaimerContent: 'By using this application to take photos of troubled-people, you are exempt from lawful liability for infringement of privacy with one condition that you blurred all private information libne face, name-place or private information.\n',
            edit_screen_zoomin: '+',
            edit_screen_zoomout: '-',
            edit_screen_del: 'X',
            edit_screen_next: 'Next',
            edit_screen_previous: 'Previous'
        },
        {
            back: '戻る',
            caution: '注意',
            yes: 'はい',
            no: 'いいえ',
            ok: 'はい',
            start_screen_familyMode: '家族モード',
            start_screen_supporterMode: 'サポーターモード',
            caution_screen_disclaimer: '免責',
            caution_screen_guideline: 'ガイドライン',
            edit_screen_question: '画像に顔が入っていますか',
            information_screen_familyID: '家族ID',
            information_screen_email: 'メルアド',
            information_screen_sex: '性別',
            information_screen_age: '年齢',
            information_screen_upclothes: '上着',
            information_screen_lowclothes: 'ズボン',
            information_screen_fullclothes: 'ワンピース・パジャマ',
            information_screen_hat: '帽子',
            information_screen_bag: 'バッグ',
            information_screen_shoe: '靴',
            information_screen_color: 'カラー',
            information_screen_send: '送信',
            information_screen_baseLineData: {
                sex: [
                    '未選択',
                    '男性',
                    '女性'
                ],
                age: [
                    '未選択',
                    '10代',
                    '20代',
                    '30代',
                    '40代',
                    '50代',
                    '60代',
                    '70代以上'
                ],
                upClothes: [
                    '未選択',
                    'ジャケット・コート',
                    'スーツ',
                    'セーター',
                    'シャツ・ブラウス',
                    'Tシャツ'
                ],
                lowClothes: [
                    '未選択',
                    'ジーンズ',
                    'パンツ',
                    'Tシャツ',
                    'スカート'
                ],
                fullClothes: [
                    '未選択',
                    'ドレス',
                    'つなぎパジャマ'
                ],
                hat: [
                    '未選択',
                    '有',
                    '無'
                ],
                bag: [
                    '未選択',
                    '有',
                    '無'
                ],
                shoe: [
                    '未選択',
                    '靴',
                    'ブーツ',
                    '無'
                ],
                color: [
                    '未選択',
                    '赤',
                    'オレンジ',
                    '黄',
                    'パープル',
                    '白',
                    '緑',
                    '青',
                    '黒',
                    'グレー'
                ]
            },
            camera_screen_saving: '保存中',
            information_screen_sending: '送信中',
            information_screen_can_not_get_location: '位置情報サービスをオンにしてください',
            message_box_send_successfully: (modeIdx)=>{
                return modeIdx === mode.supporter ? '画像を送信しました。 \nご協力ありがとうございます！' : '画像を送信しました。'
            },
            message_box_send_failed: (err)=>{
                return `${err}、画像が送信できませんでした。再度送信しますか。`
            },
            message_box_fillout: (arr)=>{
                return `${arr.join("と")}が入力してください。 `
            },
            email_is_not_valid: "メルアドは正しく入力してください。",
            caution_screen_guidelineContent: ' ',//'Please help us by sending information of troubled-people as below:\nStep 1:\nTake a picture of troubled-people with this application.\nStep 2:\nBlur all private informations like face, name-place...\nStep 3:\nAdd information about year old, clothes, shapes and other information eg hat, identity character...\nStep 4:\nSend to our server by click on Send button.',
            caution_screen_disclaimerContent: ' ',//'By using this application to take photos of troubled-people, you are exempt from lawful liability for infringement of privacy with one condition that you blurred all private information libne face, name-place or private information.\n',
            edit_screen_zoomin: '+',
            edit_screen_zoomout: '-',
            edit_screen_del: 'X',
            edit_screen_next: '次へ',
            edit_screen_previous: '取り消す'
        }
    ]
}
exports.language = language.data[language.current];
exports.languagePackage = language.data;

