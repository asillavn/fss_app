import React from 'react';
import {
    View,
    Text,
    TextInput,
    Dimensions,
    NativeModules
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';
import MainLayout from '../components/MainLayout';
import Dropdown from '../components/Dropdown';
import MyButton from '../components/MyButton';
import log from '../util/log';
import RNFetchBlob from 'react-native-fetch-blob';
import DeviceInfo from 'react-native-device-info';

class informationPage extends React.Component {
    constructor(props) {
        super(props);
        this.renderRowWithTextInput = this.renderRowWithTextInput.bind(this);
        this.state = {
            //imgBase64: global.imgBase64,
            familyID: '',
            email: '',
            sex: {
                selectedIndex: 0,
                selectedValue: baselineData.sex[0]
            },
            age: {
                selectedIndex: 0,
                selectedValue: baselineData.age[0]
            },
            upClothes: {
                selectedIndex: 0,
                selectedValue: baselineData.upClothes[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            lowClothes: {
                selectedIndex: 0,
                selectedValue: baselineData.lowClothes[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            fullClothes: {
                selectedIndex: 0,
                selectedValue: baselineData.fullClothes[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            hat: {
                selectedIndex: 0,
                selectedValue: baselineData.hat[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            bag: {
                selectedIndex: 0,
                selectedValue: baselineData.bag[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            shoe: {
                selectedIndex: 0,
                selectedValue: baselineData.shoe[0],
                color: {
                    selectedIndex: -1,
                    selectedValue: undefined,
                },
            },
            message: {
                content: '',
                buttons: []
            },
            layout: {
                height: config.windowHeight,
                width: config.windowWidth
            },
            logContent: {history: []},
            footerHeight: 0,
            sendingPercent: 0,
            dropdown: undefined,
            lastInfo: undefined, // biến này bằng undefined thì user sử dụng lần đầu tiên, nếu không thì nó chứa thông tin email, familyID lần cuối cùng user nhập
            createEventInfo: {
                isRunning: true,
                error: undefined
            }
        }

        log.read((logContent) => {
            if (!logContent || !logContent.history) {
                logContent = {
                    history: []
                }
            }
            this.setState({logContent: logContent});
            logContent = logContent.history;
            let lastModeLogIdx = -1;
            for (i = logContent.length - 1; i >= 0; i--) {
                if (logContent[i].mode === this.props.navigation.state.params.mode) {
                    lastModeLogIdx = i;
                    break;
                }
            }

            if (lastModeLogIdx > -1) {
                let info = {
                    email: logContent[lastModeLogIdx].email
                }
                this.setState({email: logContent[lastModeLogIdx].email});
                if (this.props.navigation.state.params.mode === config.mode.family) {
                    this.setState({familyID: logContent[lastModeLogIdx].familyID});
                    info.familyID = logContent[lastModeLogIdx].familyID;
                }

                this.setState({lastInfo: info});
            }
        })
    }

    componentWillMount() {
        this.preSendToServer = this.preSendToServer.bind(this);
        this.createEvent = this.createEvent.bind(this);
        this.sendToServer = this.sendToServer.bind(this);
        //this.geoLocation = this.geoLocation.bind(this);
        //this.imageToBase64 = this.imageToBase64.bind(this);
    }

    async imageToBase64() {
        RNFetchBlob.fs.readFile(this.props.navigation.state.params.imgPath, 'base64')
            .then((data) => {
                this.setState({imgBase64: data});
            })
    }

    preSendToServer() {
        if (!this.state.email || (this.props.navigation.state.params.mode === config.mode.family && !this.state.familyID)) {
            let messageContent = [];
            if (this.props.navigation.state.params.mode === config.mode.family && !this.state.familyID) {
                messageContent.push(config.language.information_screen_familyID);
            }
            if (!this.state.email) {
                messageContent.push(config.language.information_screen_email);
            }

            messageContent = config.language.message_box_fillout(messageContent);

            this.setState({
                message: {
                    content: messageContent,
                    buttons: [{
                        title: config.language.ok,
                        onClick: () => {
                            this.setState({message: null})
                        }
                    }]
                }
            })
            return;
        }
        if (!validateEmail(this.state.email)) {
            this.setState({
                message: {
                    content: config.language.email_is_not_valid,
                    buttons: [{
                        title: config.language.ok,
                        onClick: () => {
                            this.setState({message: null})
                        }
                    }]
                }
            })
            return;
        }

        let data = this.props.navigation.state.params;
        let state = JSON.parse(JSON.stringify(this.state));

        if ((data.mode !== null && data.mode !== undefined) && global.imgBase64) {
            //this.setState({message: {
            //    content: 'Preparing data for send to server.',
            //}})
            let logContent = this.state.logContent;
            // TO DO HERE: Send to server IoT K5
            this.setState({
                message: {
                    content: `${config.language.information_screen_sending}`,
                }
            })

            // var tryTime = 0;
            // while (!global.imgBase64) {
            //     //////console.log('Data still not load');
            //     setTimeout(() => {
            //         tryTime++
            //     }, 500)
            //     if (tryTime === 10) {
            //         this.setState({
            //             message: {
            //                 content: "Can't read image data",
            //                 buttons: [
            //                     {
            //                         title: config.language.ok,
            //                         onClick: () => {
            //                             this.setState({message: null})
            //                         }
            //                     }
            //                 ]
            //             }
            //         })
            //         return;
            //     }
            // }

            NativeModules.GISmodule.getCurrentLocation().then((loc) => {
                ////console.log(location);
                if (loc && !loc.hasOwnProperty("enabled") && loc.hasOwnProperty("lat") && loc.hasOwnProperty("lng")) {
                    //console.log('prepare data start: ' + (new Date).getTime())
                    // get current position success
                    let location = [loc.lng, loc.lat];
                    //console.log(location)
                    //////console.log(location)
                    let jp2En = (key) => { //only use for convert jp to en in informationPage baseline data
                        let idx = state[key].selectedIndex;
                        return config.languagePackage[0]['information_screen_baseLineData'][key][idx];
                    }

                    let getColor = (key) => {
                        if (state[key].color.selectedValue) {//Tránh trường hợp index -1 => hiển thị trên màn hình là color
                            if (idx = state[key].color.selectedIndex) {//Tránh trường hợp index 0 => unknow
                                return config.languagePackage[0]['information_screen_baseLineData'].color[idx];
                            }
                        }
                        return undefined;
                    }

                    let commonData = {
                        sex: state.sex.selectedIndex ? (state.sex.selectedIndex === 2 ? 0 : 1).toString() : undefined,
                        age: state.age.selectedIndex ? (state.age.selectedIndex * 10).toString() : undefined,
                        clothes: [
                            {
                                type: state.upClothes.selectedIndex ? jp2En('upClothes') : undefined,
                                color: getColor('upClothes')
                            },
                            {
                                type: state.lowClothes.selectedIndex ? jp2En('lowClothes') : undefined,
                                color: getColor('lowClothes'),
                            },
                            {
                                type: state.fullClothes.selectedIndex ? jp2En('fullClothes') : undefined,
                                color: getColor('fullClothes'),
                            },
                            {
                                type: state.hat.selectedIndex ? jp2En('hat') : undefined,
                                color: getColor('hat'),
                            },
                            {
                                type: state.bag.selectedIndex ? jp2En('bag') : undefined,
                                color: getColor('bag'),
                            },
                            {
                                type: state.shoe.selectedIndex ? jp2En('shoe') : undefined,
                                color: getColor('shoe')
                            },
                        ]
                    }

                    commonData.clothes = commonData.clothes.filter((item) => {
                        return item.type && item.color;
                    })

                    //////console.log(commonData)

                    if (data.mode === config.mode.family) {
                        data = {
                            family_unique_id: state.familyID,
                            family_mail: state.email,
                            family_img_base64: global.imgBase64,
                            family_img_location: {
                                type: "Point",
                                coordinates: location
                            },
                            family_img_features: commonData
                        }
                    }
                    else {
                        let dt = new Date();
                        let milliSeconds = Date.now() + (dt.getTimezoneOffset() * 60 * 1000) + 9 * 3600000;
                        ; // 9 * 3600 * 1000. 9 is Japanese GMT
                        dt = new Date(milliSeconds);
                        dt = {
                            year: dt.getFullYear(),
                            month: ("0" + (dt.getMonth() + 1)).slice(-2),
                            day: ("0" + dt.getDate()).slice(-2),
                            hour: ("0" + (dt.getHours())).slice(-2),
                            minutes: ("0" + (dt.getMinutes())).slice(-2),
                            second: ("0" + (dt.getSeconds())).slice(-2),
                            //offset: dt.getTimezoneOffset(),
                            now: milliSeconds
                            //milliSecond: dt.getMilliseconds(),
                            //timeZone: `${dt.getHours() > dt.getHours() ? '+' : '-'}` + ("0" + (Math.abs((dt.getTimezoneOffset() / -60)))).slice(-2) + '00',
                        }
                        let deviceID = DeviceInfo.getUniqueID();
                        data = {
                            img_unique_id: `${deviceID}-${parseInt(dt.now / 1000)}`,
                            supporter_mail: state.email,
                            supporter_uuid: deviceID,
                            supporter_img_base64: global.imgBase64,
                            //supporter_img_access_code: "MachimimaAC_CDLRU",
                            supporter_img_format: 'image/jpeg',
                            supporter_img_time: `${dt.year}-${dt.month}-${dt.day} ${dt.hour}:${dt.minutes}:${dt.second}`,
                            supporter_img_utc_time: parseInt(dt.now / 1000),
                            supporter_img_location: {
                                type: "Point",
                                coordinates: location
                            },
                            supporter_img_features: commonData
                        }
                    }

                    let dataSize = config.request.sizeof(data) / (1024 * 1024);

                    //////console.log(data);
                    //////console.log(JSON.stringify(data));
                    if (dataSize <= config.request.maxPackageSize) {
                        let baseLineUrl = 'http://iot-api-1.jp-east-1.paas.cloud.global.fujitsu.com/v1/4BE70DJP00/';
                        //let baseLineUrl = 'https://iot-api-1.jp-east-1.paas.cloud.global.fujitsu.com/v1/DEDMFNJP00/';
                        let resource_url = this.props.navigation.state.params.mode === config.mode.family ? 'machimima_dev/family_images' : 'machimima_dev/supporters_images';
                        let transfer_resource_url = this.props.navigation.state.params.mode === config.mode.family ? '_fwd/machimima_dev/family_api' : '_fwd/machimima_dev/supporter_api';
                        let resource_access_code = this.props.navigation.state.params.mode === config.mode.family ? 'Bearer MachimimaDevFamilyRU' : 'Bearer MachimimaDevSupporterRU';
                        let transfer_access_code = 'Bearer MachimimaDevTransferRU';

                        //console.log('prepare data end: ' + (new Date).getTime())
                        this.createEvent(_ => this.sendToServer(baseLineUrl, resource_url, transfer_resource_url, resource_access_code, transfer_access_code, data, logContent));
                    }
                    else {
                        this.setState({
                            message: {
                                content: config.language.message_box_send_failed('Data size is too large for send to server.'),
                                buttons: [
                                    {
                                        title: config.language.yes,
                                        onClick: () => {
                                            this.setState({message: null})

                                            this.preSendToServer();
                                        }
                                    },
                                    {
                                        title: config.language.no,
                                        onClick: () => {
                                            this.setState({message: null})
                                        }
                                    }
                                ]
                            }
                        })
                    }
                }
                else {
                    this.setState({
                        message: {
                            content: config.language.information_screen_can_not_get_location,
                            buttons: [
                                {
                                    title: config.language.yes,
                                    onClick: () => {
                                        this.setState({message: null})
                                        this.preSendToServer();
                                    }
                                },
                                {
                                    title: config.language.no,
                                    onClick: () => {
                                        this.setState({message: null})
                                    }
                                }
                            ]
                        }
                    })
                }
            }).catch(err => {
            })
        }
        else {
            this.setState({
                message: {
                    content: config.language.message_box_send_failed("Mode or image is wrong to read"),
                    buttons: [
                        {
                            title: config.language.yes,
                            onClick: () => {
                                this.setState({message: null})
                                this.preSendToServer();
                            }
                        },
                        {
                            title: config.language.no,
                            onClick: () => {
                                this.setState({message: null})
                            }
                        }
                    ]
                }
            })
        }
    }

    // geoLocation(successCb, errorCb){
    //     let onSuccess = (pos)=>{
    //         if (successCb && typeof successCb === 'function'){
    //             successCb(pos);
    //         }
    //     };
    //
    //     navigator.geolocation.getCurrentPosition(onSuccess, ()=>{
    //         navigator.geolocation.getCurrentPosition(onSuccess, (err)=> {
    //             if (errorCb && typeof errorCb === 'function') {
    //                 errorCb(err);
    //             }
    //         }, {enableHighAccuracy: false, timeout: 10000});
    //     }, {enableHighAccuracy: true, timeout: 10000});
    // }

    sendToServer(baseLineUrl, resource_url, transfer_resource_url, resource_access_code, transfer_access_code, data, logContent) {
        let errorFnHandle = (errorMessage, statusCode, step) => {
            //////console.log(step);
            //////console.log(statusCode, errorMessage);
            //console.log(errorMessage)
            this.setState({
                message: {
                    content: config.language.message_box_send_failed(getResponseMessage(statusCode)),
                    buttons: [
                        {
                            title: config.language.yes,
                            onClick: () => {
                                //step === 'step1' ? step1() : step2();  // recall step 1 or 2 if this step execute fail
                                step2();
                                this.setState({message: null});
                            }
                        },
                        {
                            title: config.language.no,
                            onClick: () => {
                                this.setState({message: null})
                            }
                        }
                    ]
                }
            })

            logContent.history.push({
                datetime: new Date(),
                status: statusCode,
                exception: {
                    from: step,
                    error: errorMessage
                },
                mode: this.props.navigation.state.params.mode,
                familyID: this.state.familyID,
                email: this.state.email
            });
            log.write(logContent);
        }

        let step2 = () => {
            ////console.log(transfer_access_code)
            ////console.log(JSON.stringify(data))
            //console.log('sending data to ' + baseLineUrl + transfer_resource_url + ' start: ' + (new Date).getTime())

            RNFetchBlob.fetch('PUT', baseLineUrl + transfer_resource_url, {
                'Content-Type': 'application/json',
                'Authorization': transfer_access_code
            }, JSON.stringify(data)).then((res) => {
                //console.log('sending data to ' + baseLineUrl + transfer_resource_url + ' end: ' + (new Date).getTime())
                let statusCode = res.info().status;
                //////console.log(res.info())
                //////console.log(res)

                if (statusCode === 200 || statusCode === 201) {
                    // Delete all images and information then warning to user
                    //let fileName = this.props.navigation.state.params.imgPath.substring(this.props.navigation.state.params.imgPath.lastIndexOf('/'), this.props.navigation.state.params.imgPath.length);
                    ////////console.log(fileName);
                    ////////console.log(this.props.navigation.state.params.ExternalStorageDirectoryPath);
                    //RNFetchBlob.fs.unlink(`${this.props.navigation.state.params.ExternalStorageDirectoryPath}/Pictures/${fileName}`)
                    //   .then(() => { //////console.log('Image was deleted') })
                    //  .catch((err) => { //////console.log("Image didn't deleted" + err) })

                    this.setState({
                        message: {
                            content: config.language.message_box_send_successfully(this.props.navigation.state.params.mode),
                            buttons: [
                                {
                                    title: config.language.ok,
                                    onClick: () => {
                                        this.props.navigation.goBack(this.props.navigation.state.params.go_back_key)
                                    }
                                }
                            ]
                        }
                    })

                    // Write log
                    logContent.history.push({
                        datetime: new Date(),
                        status: statusCode,
                        mode: this.props.navigation.state.params.mode,
                        familyID: this.state.familyID,
                        email: this.state.email
                    });
                    log.write(logContent);
                }
                else {
                    errorFnHandle(res.data, statusCode, 'step2');
                }
            }).catch((errorMessage, statusCode) => {
                errorFnHandle(errorMessage, statusCode, 'step2');
            })
        }
        step2();

        /*if(this.state.createEventInfo.error){
         this.setState({message: {
         content: config.language.message_box_send_failed(this.state.createEventInfo.error),
         buttons: [
         {
         title: config.language.ok,
         onClick: ()=>{this.setState({message: null})}
         }
         ]
         }})

         return;
         }*/

        // Bỏ xử lý step1 theo flow ngày 06/09/2017
        /*let step1 = ()=>{
         RNFetchBlob.fetch('PUT', baseLineUrl + resource_url, {
         'Content-Type' : 'application/json',
         'Authorization': resource_access_code
         }, JSON.stringify(data)).then((res)=>{
         let statusCode = res.info().status;
         //////console.log(res.info())
         //////console.log(res)

         if(statusCode === 200){
         // Delete all images and information then warning to user
         step2();
         }
         else{
         errorFnHandle(res.data, statusCode, 'step1');
         }
         }).catch((errorMessage, statusCode)=>{
         errorFnHandle(errorMessage, statusCode, 'step1');
         })
         }
         step1();
         */
    }

    createEvent(cbFn) {
        //////console.log(this.props.navigation.state.params.mode);
        if (/*validateEmail(this.state.email) && */
            (!this.state.lastInfo || // Check last info null
            this.state.lastInfo.email !== this.state.email || // check email bi thay doi
            (this.props.navigation.state.params.mode === config.mode.family && this.state.lastInfo.familyID !== this.state.familyID))) { // check family id bi thay doi neu o mode family
            this.setState({
                createEventInfo: {
                    isRunning: true
                }
            })
            //console.log('create event start: ' + (new Date).getTime())
            RNFetchBlob.fetch('POST', 'http://52.39.39.160:5000/create_event', {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ZnNzX2FwaTpha3ppcUBhOTFrJA=='
            }, JSON.stringify({
                uid: this.props.navigation.state.params.mode === config.mode.family ? this.state.familyID : DeviceInfo.getUniqueID(),
                mail: this.state.email,
                type: this.props.navigation.state.params.mode.toString()
            })).then((res) => {
                //console.log('create event end: ' + (new Date).getTime())
                let statusCode = res.info().status;
                let resData = JSON.parse(res.data);
                //////console.log(res.info());
                //////console.log(resData);

                let createEventInfo = {
                    isRunning: false,
                };

                if (statusCode === 200) {
                    if (resData && resData.status !== undefined && resData.status !== null && (resData.status === 200 || resData.status === 201 || resData.status === 0)) {

                    }
                    else {
                        if (resData && resData.staus) {
                            createEventInfo.error = "Asilla status code: " + resData.status;
                        }
                        else {
                            createEventInfo.error = "Other info";
                        }
                    }
                }
                else {
                    createEventInfo.error = "Status code: " + statusCode;
                }

                this.setState({createEventInfo: createEventInfo});
                cbFn();
            }).catch((errorMessage, statusCode) => {
                this.setState({
                    createEventInfo: {
                        isRunning: false,
                        error: errorMessage
                    }
                });
                cbFn();
            })
        }
        else {
            //////console.log('nothing change');
            cbFn();
        }
    }

    renderRowWithTextInput(title, value, onChangeText, onBlur) {
        return (
            <View style={{paddingTop: 5, paddingBottom: 5}}>
                <View style={{marginBottom: 3}}>
                    <View>
                        <Text
                            style={[theme.informationTitleFont, {width: this.state.layout.width * .35}]}>{title}</Text>
                    </View>
                </View>
                <View>
                    <TextInput underlineColorAndroid="transparent"
                               multiline={false}
                               style={theme.textBox}
                               value={value}
                               onChangeText={onChangeText}
                               onBlur={onBlur}
                               returnKeyType={'done'}
                               blurOnSubmit={true}
                    />
                </View>
            </View>
        )
    }

    renderRowWithCombobox(title, dropdowns) {
        return (
            <View style={{paddingTop: 5, paddingBottom: 5}}>
                <View style={{marginBottom: 3}}>
                    <View>
                        <Text style={[theme.informationTitleFont, {width: this.state.layout.width * .9}]}>{title}</Text>
                    </View>
                </View>
                <View>
                    <View style={theme.row}>
                        {
                            dropdowns.map((dropdown, idx) => {
                                return (
                                    <View style={theme.col} key={idx}>
                                        <View
                                            style={dropdowns.length > 1 ? idx === 0 ? {paddingRight: 5} : idx === dropdowns.length - 1 ? {paddingLeft: 5} : {
                                                paddingLeft: 5,
                                                paddingRight: 5
                                            } : {}}>
                                            <Dropdown style={theme.comboBox}
                                                      width={(this.state.layout.width - 20) / dropdowns.length}
                                                      selectedValue={dropdown.selectedValue} onClick={dropdown.onClick}
                                                      placeholder={dropdown.title}/>
                                        </View>
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
            </View>
        )
    }

    render() {
        let dropdownBackdropClick = () => {
            this.setState({dropdown: undefined});
        }
        return (
            <MainLayout showCaution={false} showBack={true} navigationHandle={this.props.navigation}
                        width={this.state.layout.width} height={this.state.layout.height} onLayout={(e) => {
                let {width, height} = Dimensions.get('window');
                this.setState({
                    layout: {
                        width: width,
                        height: height
                    }
                })
            }} message={this.state.message} footer={
                <View style={{padding: 20, alignItems: 'center'}} onLayout={(e) => {
                    this.setState({footerHeight: e.nativeEvent.layout.height})
                }}>
                    <MyButton title={config.language.information_screen_send} width={this.state.layout.width * .8}
                              onClick={() => this.preSendToServer()}/>
                </View>
            } footerHeight={this.state.footerHeight} dropdown={this.state.dropdown}>
                <View style={{padding: 10, backgroundColor: '#fff'}}>
                    {
                        this.props.navigation.state.params.mode === config.mode.family ? (
                            this.renderRowWithTextInput(config.language.information_screen_familyID, this.state.familyID, (val) => {
                                this.setState({familyID: val})
                            })) : (null)
                    }
                    {
                        this.renderRowWithTextInput(config.language.information_screen_email, this.state.email, (val) => {
                            this.setState({email: val})
                        })
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_sex, [
                            {
                                title: config.language.information_screen_sex,
                                selectedValue: this.state.sex.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.sex,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_sex,
                                            selectedIndex: this.state.sex.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    sex: {
                                                        selectedIndex: idx,
                                                        selectedValue: val
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_age, [
                            {
                                title: config.language.information_screen_age,
                                selectedValue: this.state.age.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.age,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_age,
                                            selectedIndex: this.state.age.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    age: {
                                                        selectedIndex: idx,
                                                        selectedValue: val
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_upclothes, [
                            {
                                title: config.language.information_screen_upclothes,
                                selectedValue: this.state.upClothes.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.upClothes,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_upclothes,
                                            selectedIndex: this.state.upClothes.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    upClothes: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.upClothes.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.upClothes.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.upClothes.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    upClothes: {
                                                        selectedIndex: this.state.upClothes.selectedIndex,
                                                        selectedValue: this.state.upClothes.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_lowclothes, [
                            {
                                title: config.language.information_screen_lowclothes,
                                selectedValue: this.state.lowClothes.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.lowClothes,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_lowclothes,
                                            selectedIndex: this.state.lowClothes.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    lowClothes: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.lowClothes.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.lowClothes.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.lowClothes.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    lowClothes: {
                                                        selectedIndex: this.state.lowClothes.selectedIndex,
                                                        selectedValue: this.state.lowClothes.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_fullclothes, [
                            {
                                title: config.language.information_screen_fullclothes,
                                selectedValue: this.state.fullClothes.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.fullClothes,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_fullclothes,
                                            selectedIndex: this.state.fullClothes.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    fullClothes: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.fullClothes.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.fullClothes.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.fullClothes.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    fullClothes: {
                                                        selectedIndex: this.state.fullClothes.selectedIndex,
                                                        selectedValue: this.state.fullClothes.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_hat, [
                            {
                                title: config.language.information_screen_hat,
                                selectedValue: this.state.hat.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.hat,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_hat,
                                            selectedIndex: this.state.hat.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    hat: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.hat.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.hat.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.hat.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    hat: {
                                                        selectedIndex: this.state.hat.selectedIndex,
                                                        selectedValue: this.state.hat.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_bag, [
                            {
                                title: config.language.information_screen_bag,
                                selectedValue: this.state.bag.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.bag,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_bag,
                                            selectedIndex: this.state.bag.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    bag: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.bag.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.bag.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.bag.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    bag: {
                                                        selectedIndex: this.state.bag.selectedIndex,
                                                        selectedValue: this.state.bag.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                    {
                        this.renderRowWithCombobox(config.language.information_screen_shoe, [
                            {
                                title: config.language.information_screen_shoe,
                                selectedValue: this.state.shoe.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.shoe,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_shoe,
                                            selectedIndex: this.state.shoe.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    shoe: {
                                                        selectedIndex: idx,
                                                        selectedValue: val,
                                                        color: this.state.shoe.color
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            },
                            {
                                title: config.language.information_screen_color,
                                selectedValue: this.state.shoe.color.selectedValue,
                                onClick: () => {
                                    this.setState({
                                        dropdown: {
                                            options: baselineData.color,
                                            width: this.state.layout.width,
                                            height: this.state.layout.height,
                                            title: config.language.information_screen_color,
                                            selectedIndex: this.state.shoe.color.selectedIndex,
                                            backdropClick: dropdownBackdropClick,
                                            onSelect: (idx, val) => {
                                                this.setState({
                                                    shoe: {
                                                        selectedIndex: this.state.shoe.selectedIndex,
                                                        selectedValue: this.state.shoe.selectedValue,
                                                        color: {
                                                            selectedIndex: idx,
                                                            selectedValue: val
                                                        }
                                                    }
                                                })
                                                dropdownBackdropClick();
                                            }
                                        }
                                    })
                                }
                            }
                        ])
                    }
                </View>
            </MainLayout>
        )
    }
}

export default informationPage

function validateEmail(email) {
    email = email.trim();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function getResponseMessage(statusCode) {
    switch (statusCode) {
        case 400:
            mess = 'リクエストデータに不正値があるため';
            break;
        case 401:
        case 403:
            mess = 'サーバへのアクセス権がないため';
            break;
        case 404:
            mess = 'リソースが存在しないため';
            break;
        case 405:
            mess = '該当のメソッドタイプは許可されていないため';
            break;
        case 408:
            mess = 'リクエストタイムアウトのため';
            break;
        case 409:
            mess = '他のリソースと競合しているため';
            break;
        case 411:
        case 412:
        case 413:
        case 414:
        case 415:
        case 416:
            mess = 'サーバアクセスを拒否したため';
            break;
        case 423:
            mess = 'リソースがロックされているため';
            break;
        case 429:
            mess = '契約上のトラフィック上限を超えているため';
            break;
        case 495:
            mess = '無効なクライアント証明書を受信したため';
            break;
        case 500:
            mess = 'サーバ側に問題が発生しているため';
            break;
        case 501:
            mess = 'サーバで未サポートリクエストのメソッドが送信されたため';
            break;
        case 502:
            mess = 'ゲートウェイサーバが起動しているため';
            break;
        case 503:
            mess = '一時的にサーバへアクセスできないため';
            break;
        default:
            mess = 'エラーが発生したため';
            break;
    }

    return mess;
}

const row = {
    padding: 0,
}
const col = {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 0,
    paddingRight: 0
}

const baselineData = config.language.information_screen_baseLineData;