import React from 'react';
import
{
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableHighlight,
    PanResponder
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';
import MainLayout from '../components/MainLayout';
import MyButton from "../components/MyButton";
import { takeSnapshot/*, dirs*/ } from "react-native-view-shot";
//const { PictureDir } = dirs;

const defaultHeartSize = 128;

class editPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            layout: {
                height: config.windowHeight,
                width: config.windowWidth
            },
            footerHeight: 0,
            hearts: [],
            selectedIndexHeart: -1,
            message: {
                content: '',
                buttons: []
            },
            scrollable: true,
        }
        this.renderFooter = this.renderFooter.bind(this);
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {
                //////console.log('Start');
                //////console.log(gestureState);
                let hearts = this.state.hearts.slice();
                let idx = this.state.selectedIndexHeart;
                hearts[idx].lastX = gestureState.x0 - hearts[idx].X;
                hearts[idx].lastY = gestureState.y0 - hearts[idx].Y;
                this.setState({
                    hearts: hearts
                })
            },
            onPanResponderMove: (evt, gestureState) => {
                //////console.log('Move');
                //////console.log(gestureState);
                let hearts = this.state.hearts.slice();
                let idx = this.state.selectedIndexHeart;
                hearts[idx].X = gestureState.moveX - hearts[idx].lastX;
                hearts[idx].Y = gestureState.moveY - hearts[idx].lastY;
                this.setState({
                    hearts: hearts
                })
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                //this.setState({selectedIndexForMovingHeart: -1})
                this.setState({scrollable: true})
            },
            onPanResponderTerminate: (evt, gestureState) => {
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                return true;
            },
        });
    }

    renderRemoveButton(){
        let {X, Y, scale} = this.state.hearts[this.state.selectedIndexHeart];
        let size = defaultHeartSize * scale + 4;
        let position = {
            left: X + size,
            top: Y
        };
        let recSize = 42;
        //Check position của trái tim để tìm ra chỗ hợp lý
        if(Y < 0){
            position.top = Y + size - recSize;
            if (position.top < 0){
                position.top = Y + size;
                if (X + size + recSize > this.state.layout.width) {
                    position.left = X;
                }
                else{
                    position.left = X + size - recSize;
                }
            }
            else if (X + size + recSize > this.state.layout.width){
                position.left = X - recSize;
            }
        }
        else if (position.top + recSize > this.state.layout.height){
            position.top -= recSize;
            position.left = X + recSize - recSize;
            if (X + size + recSize > this.state.layout.width){
                position.left = X;
            }
            else{
                position.left = X + size - recSize;
            }
        }
        else{
            if (X + size + recSize > this.state.layout.width){
                position.left = X - recSize;
            }
        }

        return (
            <TouchableOpacity style={{padding: 5, backgroundColor: config.secondaryColor, position: 'absolute', ...position}} onPress={()=>{
                if(this.state.selectedIndexHeart > -1){
                    let hearts = this.state.hearts.slice();
                    hearts.splice(this.state.selectedIndexHeart, 1);
                    this.setState({selectedIndexHeart: -1});
                    this.setState({hearts: hearts});
                }
            }}>
                <Image source={require('../images/delete.png')} style={{height: 32, width: 32}}/>
            </TouchableOpacity>
        )
    }

    renderFooter(){
        if(this.state.selectedIndexHeart > -1){
            let hearts = this.state.hearts.slice();
            let heart = hearts[this.state.selectedIndexHeart];
            let zoomChange = (isZoomIn) => {
                heart.scale += isZoomIn ? + .1 : -.1;
                this.setState({hearts: hearts});
            }
            return (
                <View style={{paddingTop: theme.editPageFooterTableLayout.paddingBottom}}>
                    <View style={[theme.editPageFooterTableLayout, theme.row]}>
                        <View style={[theme.col, theme.horizontalContent]}>
                            <MyButton title={config.language.edit_screen_zoomout} disabled={heart.scale.toFixed(1) <= 0.5} width={this.state.layout.width * .4} onClick={()=>zoomChange(false)}/>
                        </View>
                        <View style={[theme.col, theme.horizontalContent]}>
                            <MyButton style={theme.horizontalContent} disabled={heart.scale.toFixed(1) >= 2} title={config.language.edit_screen_zoomin} width={this.state.layout.width * .4} onClick={()=>zoomChange(true)}/>
                        </View>
                    </View>
                </View>
            )
        }

        return (
            <View style={{paddingTop: theme.editPageFooterTableLayout.paddingBottom}}>
                <View style={[theme.editPageFooterTableLayout, theme.row]}>
                    <View style={[theme.col, theme.horizontalContent]}>
                        <MyButton style={theme.horizontalContent} title={config.language.edit_screen_previous} width={this.state.layout.width * .45} onClick={()=>{
                            this.props.navigation.goBack();
                        }}/>
                    </View>
                    <View style={[theme.col, theme.horizontalContent]}>
                        <MyButton title={config.language.edit_screen_next} width={this.state.layout.width * .45} onClick={()=>{
                            let params = this.props.navigation.state.params;
                            params.go_back_key = this.props.navigation.state.key;
                            if(this.state.hearts && this.state.hearts.length){
                                this.setState({selectedIndexHeart: -1})
                                this.setState({
                                    message: {
                                        content: config.language.camera_screen_saving
                                    }
                                })
                                //let filename = this.props.navigation.state.params.imgPath.substring(this.props.navigation.state.params.imgPath.lastIndexOf('/'), this.props.navigation.state.params.imgPath.lastIndexOf('.')) + '_new.jpg';
                                takeSnapshot(this.refs.image, {
                                    format: "jpeg",
                                    //path: PictureDir + filename,
                                    quality: .8,
                                    result: "base64"
                                }).then(
                                    (base64encoded) => {
                                        this.setState({
                                            message: undefined
                                        })
                                        global.imgBase64 = base64encoded;
                                        this.props.navigation.navigate('informationPage', params);
                                    },
                                    error => {
                                        //////console.log("Oops, snapshot failed", error);
                                        this.setState({
                                            message: undefined
                                        })
                                    }
                                );
                            }
                            else{
                                this.props.navigation.navigate('informationPage', params);
                            }
                        }}/>
                    </View>
                </View>
            </View>
        )
    }

    render(){
        var panHandler = this.state.scrollable ? null:this._panResponder.panHandlers;
        return (
            <MainLayout showCaution={false} showBack={true} navigationHandle={this.props.navigation} footer={
                <View style={[theme.editPageFooter, {
                    bottom: 0,
                    left: 0,
                    width: this.state.layout.width,
                }]} onLayout={(e)=>{
                    this.setState({footerHeight: e.nativeEvent.layout.height})
                }}>
                    {
                        this.renderFooter()
                    }
                </View>
            } footerHeight={this.state.footerHeight} width={this.state.layout.width} height={this.state.layout.height} onLayout={(e)=>{
                let {width, height} = Dimensions.get('window');
                this.setState({
                    layout: {
                        width: width,
                        height: height
                    }
                })
            }} message={this.state.message} tip={'タップすると顔を隠すハートを表示します'} scrollEnabled={this.state.scrollable}>
                <View ref="image" collapsable={false} {...panHandler} >
                    <Image source={{uri: `data:image/jpg;base64,${global.imgBase64}`}} style={{
                        height: (this.state.layout.width * 1280) / 720,
                        width: this.state.layout.width
                    }}>
                        <TouchableOpacity style={{width: this.state.layout.width, height: this.state.layout.height, backgroundColor: 'transparent'}} onPress={(e)=>{
                            let hearts = this.state.hearts.slice();
                            hearts.push({
                                X: e.nativeEvent.locationX - defaultHeartSize / 2,
                                Y: e.nativeEvent.locationY - defaultHeartSize / 2,
                                lastX: e.nativeEvent.locationX - defaultHeartSize / 2,
                                lastY: e.nativeEvent.locationY - defaultHeartSize / 2,
                                scale: 1,
                            });
                            this.setState({hearts: hearts});
                            this.setState({selectedIndexHeart: hearts.length - 1});
                        }}>
                            {
                                this.state.hearts.map((heart, idx)=>{
                                    let {X, Y} = heart;
                                    if(idx !== this.state.selectedIndexHeart){
                                        X += 2;
                                        Y += 2;
                                    }

                                    return (
                                        <TouchableHighlight key={idx} style={{position: 'absolute', top: Y, left: X, backgroundColor: 'transparent'}} underlayColor={'transparent'} onLongPress={()=>{
                                            this.setState({selectedIndexHeart: idx});
                                            this.setState({scrollable: false});
                                        }} onPress={()=>{
                                            if(idx === this.state.selectedIndexHeart){
                                                this.setState({selectedIndexHeart: -1});
                                            }
                                            else{
                                                this.setState({selectedIndexHeart: idx});
                                            }
                                        }}>
                                            <View>
                                                <View style={idx === this.state.selectedIndexHeart ? {borderWidth: 2, borderColor: config.secondaryColor} : null}>
                                                    <Image source={require('../images/heart.png')} style={{width: defaultHeartSize * heart.scale, height: defaultHeartSize * heart.scale}} resizeMode={'contain'}/>
                                                </View>
                                            </View>
                                        </TouchableHighlight>
                                    )
                                })
                            }
                        </TouchableOpacity>
                        {
                            this.state.selectedIndexHeart !== -1 ? this.renderRemoveButton() :(null)
                        }
                    </Image>
                </View>
            </MainLayout>
        )
    }
}

export default editPage