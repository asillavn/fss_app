import React from 'react';
import {
    View,
    Text,
    Dimensions
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';
import MainLayout from '../components/MainLayout';

class cautionPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            layout: {
                height: config.windowHeight,
                width: config.windowWidth
            }
        }
    }

    render (){
        let guideLineContent = config.language.caution_screen_guidelineContent;
        let disclaimerContent = config.language.caution_screen_disclaimerContent;
        return (
            <MainLayout showCaution={false} showBack={true} navigationHandle={this.props.navigation} width={this.state.layout.width} height={this.state.layout.height} onLayout={(e)=>{
                let {width, height} = Dimensions.get('window');
                this.setState({
                    layout: {
                        width: width,
                        height: height
                    }
                })
            }}>
                <View style={{padding: 20, backgroundColor: '#fff'}}>
                    <Text style={[theme.navFont, {color: theme.cautionContentFont.color}]}>{config.language.caution_screen_disclaimer}</Text>
                    <Text style={[theme.cautionContentFont, {
                        marginBottom: 20
                    }]}>
                        {disclaimerContent}
                    </Text>
                    <Text style={[theme.navFont, {color: theme.cautionContentFont.color}]}>{config.language.caution_screen_guideline}</Text>
                    <Text style={theme.cautionContentFont}>
                        {guideLineContent}
                    </Text>
                </View>
            </MainLayout>
        )
    }
}

export default cautionPage