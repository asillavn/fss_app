import React from 'react';
import {
    View,
    Image,
    Dimensions,
    Text
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';
import MyButton from '../components/MyButton';
import MainLayout from '../components/MainLayout';

class modePage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            layout: {
                height: config.windowHeight,
                width: config.windowWidth
            }
        }
        this.gotoCameraPage = this.gotoCameraPage.bind(this);
    }

    gotoCameraPage(mode){
        this.props.navigation.navigate('cameraPage', {mode: mode});
    }

    render (){
        return (
            <MainLayout showCaution={false} showBack={false} navigationHandle={this.props.navigation} width={this.state.layout.width} height={this.state.layout.height} onLayout={(e)=>{
                let {width, height} = Dimensions.get('window');
                this.setState({
                    layout: {
                        width: width,
                        height: height
                    }
                })
            }}>
                <View style={theme.horizontalContent}>
                    <View style={[theme.verticalContent, {height: this.state.layout.height}]}>
                        <Image source={require('../images/bg.png')} style={{marginBottom: 40, width: this.state.layout.width * .8, height: this.state.layout.width * .8 * 359 / 343}}/>
                        <MyButton title={config.language.start_screen_familyMode} width={this.state.layout.width * .8} style={{marginBottom: 20}} onClick={()=> this.gotoCameraPage(config.mode.family)}/>
                        <MyButton title={config.language.start_screen_supporterMode} width={this.state.layout.width * .8} onClick={()=> this.gotoCameraPage(config.mode.supporter)}/>
                    </View>
                </View>
            </MainLayout>
        )
    }
}

export default modePage