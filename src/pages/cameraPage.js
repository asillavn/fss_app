import {Dimensions} from 'react-native';
import React from 'react';
import config from '../appConfig';
import MainLayout from '../components/MainLayout';
import Camera from '../components/Camera';
import CaptureButton from '../components/CaptureButton'

class cameraPage extends React.Component {
    componentWillMount(){
        this.state = {
            layout: {
                height: config.windowHeight,
                width: config.windowWidth
            },
            message: {
                content: '',
                buttons: []
            }
        }
    }

    takePicture() {
        this.setState({
            message: {
                content: config.language.camera_screen_saving
            }
        })
        const options = {};
        //options.location = ...

        this.camera.capture({metadata: options})
            .then((data) => {
                if(data && data.path){
                    this.setState({
                        message: undefined
                    })
                    let mode = this.props.navigation.state.params.mode;
                    this.props.navigation.navigate('editPage', {
                        mode: mode,
                        //imgPath: data.path,
                    })

                    global.imgBase64 = data.imgBase64;
                }
            })
            .catch(err => console.error(err));
    }

    render (){
        return (
            <MainLayout showCaution={false} showBack={true} navigationHandle={this.props.navigation} backBtnCb={()=>{
                this.props.navigation.goBack()
            }} message={this.state.message} onLayout={(e)=>{
                let {width, height} = Dimensions.get('window');
                this.setState({
                    layout: {
                        width: width,
                        height: height
                    }
                })
            }} width={this.state.layout.width} height={this.state.layout.height}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={{
                        height: config.windowHeight - config.navbarHeight,
                        width: config.windowWidth
                    }}
                    captureQuality={Camera.constants.CaptureQuality["720p"]}
                    captureTarget={Camera.constants.CaptureTarget.disk}
                    aspect={Camera.constants.Aspect.fill}
                    orientation={"portrait"}>
                    <CaptureButton style={{
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        paddingBottom: 25,
                    }} onClick={this.takePicture.bind(this)}/>
                </Camera>
            </MainLayout>
        )
    }
}

export default cameraPage