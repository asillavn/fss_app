import config from './appConfig'

export default theme = {
    main: {
        backgroundColor: config.primaryColor,
    },
    navbar: {
        backgroundColor: config.primaryColor,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10
    },
    content:{
        backgroundColor: config.primaryColor
    },
    horizontalContent: {
        alignItems: 'center'
    },
    verticalContent: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    cautionTitle: {
        fontSize: config.font.navButton.fontSize,
        color: '#fff',
        fontWeight: 'bold'
    },
    cautionContent: {
        color: '#fff',
        fontSize: config.font.default.fontSize

    },
    editPageFooter: {
        backgroundColor: config.primaryColor,
        position: 'absolute',
    },
    editPageFooterTableLayout: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 10,
    },
    navFont: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: config.font.navButton.fontSize
    },
    informationTitleFont: {
        color: '#666',
        fontWeight: '600',
        fontSize: config.font.default.fontSize
    },
    row:{
        flex: 1,
        //alignSelf: 'stretch',

        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    col:{
        flex: 1,
        //alignSelf: 'stretch',
        flexDirection: 'column',
        justifyContent: 'center',
        //borderWidth: 1
    },
    textBox: {
        paddingTop: 7,
        paddingBottom: 7,
        paddingLeft: 12,
        paddingRight: 12,
        borderColor: config.primaryColor,
        borderWidth: 2,
        borderRadius: 3,
        color: '#666',
        backgroundColor: '#fff',
        fontSize: config.font.default.fontSize
    },
    comboBox: {
        backgroundColor: '#fff',
        borderColor: config.primaryColor,
        borderWidth: 2,
        borderRadius: 3,
    },
    cautionContentFont: {
        color: '#666',
        fontWeight: '600',
        fontSize: config.font.default.fontSize
    },
    heartInImage: {
        position: 'absolute',

    }
}