/**
 * Created by TrungDV on 7/26/2017.
 */
import {Platform} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';

const fs = RNFetchBlob.fs;
//const rootDir = Platform.OS === 'ios' ? fs.dirs.DocumentDir : fs.dirs.SDCardApplicationDir;
const rootDir = fs.dirs.DocumentDir;
const rootFile = `${rootDir}/logs.dat`;
const encodeType = 'utf8';

export default Log = {
    read: (cbFun)=>{
        //////console.log(rootFile)
        fs.exists(rootFile)
            .then((exist) => {
                //////console.log(exist);
                if(exist){
                    fs.readStream(rootFile, encodeType)
                        .then((stream) => {
                            let data = ''
                            stream.open()
                            stream.onData((chunk) => {
                                data += chunk
                            })
                            stream.onEnd(() => {
                                //////console.log(data);
                                if(data){
                                    data = JSON.parse(data);
                                    cbFun(data);
                                }
                                else{
                                    cbFun(undefined);
                                }
                            })
                        })
                }
                else{
                    cbFun(undefined);
                }
            }).catch(() => { })
    },
    write: (json)=>{
        json = JSON.stringify(json);
        fs.exists(rootFile)
            .then((exist) => {
                if(exist){
                    fs.writeStream(rootFile, encodeType)
                        .then((stream) => {
                            stream.write(json)
                            return stream.close()
                        })
                }
                else{
                    RNFetchBlob.fs.createFile(rootFile, json, encodeType);
                }
            }).catch(() => { })
    },
    remove: ()=>{
        fs.unlink(rootFile)
            .then(() => {
                //////console.log('deleted')
            })
            .catch((err) => {
                //////console.log(err);
            })
    }
}