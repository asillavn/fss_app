import React from 'react';

import {StackNavigator} from 'react-navigation';

import modePage from './pages/modePage';
import cautionPage from './pages/cautionPage';
import cameraPage from './pages/cameraPage';
import editPage from './pages/editPage';
import informationPage from './pages/informationPage';

const AppNavigator = StackNavigator(
    {
        modePage: {screen: modePage},
        cautionPage: {screen: cautionPage},
        cameraPage: {screen: cameraPage},
        editPage: {screen: editPage},
        informationPage: {screen: informationPage}
    },
    {
        initialRouteName: 'modePage',
        headerMode: 'none',
        navigationOptions: ({navigation, screenProps}) => (
            {
                header: null
            }
        )
    }
);

export default AppNavigator