import React from 'react';
import
{
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import config from '../appConfig';

export default class MyButton extends React.Component{
    render(){
        let {title, style, onClick, width, disabled} = this.props;
        return (
            <TouchableOpacity style={[{
                alignItems: 'center',
                width: width,
                backgroundColor: disabled ? '#ddd' : '#fff',
                paddingTop: 20,
                paddingBottom: 20,
                paddingLeft: 10,
                paddingRight: 10,
                borderRadius: 10,
                borderColor: disabled ? '#ccc':'#ddd',
                borderWidth: 1
            }, style]} onPress={onClick} disabled={disabled}>
                <View>
                    <Text style={{
                        color: disabled ? '#999' : '#666',
                        fontWeight: 'bold',
                        fontSize: config.font.myButton.fontSize
                    }}>{title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}