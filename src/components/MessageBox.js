import React from 'react';
import
{
    View,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import theme from '../appTheme';
import config from '../appConfig';

export default class MessageBox extends React.Component{
    render(){
        let {buttons, content, style, visible, width, height} = this.props;

        return visible ? (
            <ScrollView style={[{
                width: width,
                height: height,
                position: 'absolute',
                top: 0,
                left: 0,
                backgroundColor: 'rgba(0, 0, 0, .5)',
                zIndex: 99
            }, style]}>
                <View style={{
                    width: width,
                    height: height,
                    flex: 1,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <View style={{
                        width: width * .9,
                        padding: 20,
                        backgroundColor: config.secondaryColor,
                    }}>
                        <Text style={theme.navFont}>{content}</Text>
                        {
                            buttons && buttons.length ?
                                (
                                    <View style={{
                                        marginTop: 20,
                                        marginBottom: 20,
                                        height: 64
                                    }}>
                                        {
                                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: 64}}>
                                                {
                                                    buttons.map((button, idx)=>{
                                                        return (
                                                            <View style={{height: 64, alignItems: 'center', flex: 1, flexDirection: 'column'}} key={idx}>
                                                                <TouchableOpacity onPress={button.onClick} style={{
                                                                    backgroundColor: config.primaryColor,
                                                                    padding: 20,
                                                                    alignItems: 'center',
                                                                    width: 120
                                                                }}>
                                                                    <Text style={theme.navFont}>{button.title}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </View>
                                        }
                                    </View>
                                ):(null)
                        }
                    </View>
                </View>
            </ScrollView>
        ) : (null)
    }
}