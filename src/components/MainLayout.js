import React from 'react';
import
{
    View,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';
import NavButton from '../components/NavButton';
import MessageBox from '../components/MessageBox';
import SelectorModal from '../components/SelectorModal';

export default class MainLayout extends React.Component{
    componentWillMount(){
        if (this.props.tip){
            this.state = {
                tip: this.props.tip
            };
        }
    }

    render(){
        let {showCaution, showBack, navigationHandle, width, height, message, onLayout, footer, footerHeight, navRight, backBtnCb, dropdown, scrollEnabled, onMomentumScrollEnd, onScroll, handle} = this.props;
        if(!footerHeight){
            footerHeight = 0
        }
        let navbarHeight = !showCaution && !showBack ? 0 : config.navbarHeight;

        return (
            <View key={'mainLayout'} onLayout={onLayout}>
                <View style={[theme.main, {
                    height: height,
                    width: width
                }]}>
                    <StatusBar hidden/>
                    <View style={{
                        height: navbarHeight,
                        width: width
                    }}>
                        <View style={theme.navbar}>
                            <View>
                                {
                                    showBack ? (
                                        <View style={theme.verticalContent}>
                                            <NavButton title={config.language.back} onClick={()=>{
                                                if(backBtnCb){
                                                    backBtnCb();
                                                }
                                                else{
                                                    navigationHandle.goBack();
                                                }
                                            }}/>
                                        </View>
                                    ) : (null)
                                }
                            </View>
                            <View></View>
                            <View>
                                {
                                    showCaution ? (
                                        <View style={theme.verticalContent}>
                                            <NavButton title={config.language.caution} onClick={()=>{
                                                navigationHandle.navigate('cautionPage', );
                                            }}/>
                                        </View>
                                    ) :
                                    navRight ? (
                                        <View style={theme.verticalContent}>
                                            <NavButton title={navRight.title} onClick={()=>{
                                                navRight.cbFun();
                                            }}/>
                                        </View>
                                    ): (null)
                                }
                            </View>
                        </View>
                    </View>
                    <View style={{height: height - navbarHeight - footerHeight}}>
                        <ScrollView style={[theme.content, {
                            height: height - navbarHeight - footerHeight,
                            width: width,
                        }]} scrollEnabled={scrollEnabled} onMomentumScrollEnd={onMomentumScrollEnd} onScroll={onScroll} ref={(ref)=>{
                            if (handle && !handle.scrollView){
                                handle.scrollView = ref;
                            }
                        }}>
                            {
                                this.props.children
                            }
                        </ScrollView>
                        {
                            this.state && this.state.tip ?
                                (
                                    <TouchableOpacity style={{position: 'absolute', bottom: 10, width: width}} onPress={()=>{this.setState({tip: ''})}}>
                                        <View style={{...theme.horizontalContent}}>
                                            <View style={{backgroundColor: config.secondaryColor, width: width*.95, ...theme.verticalContent, paddingTop: 7, paddingBottom: 7, paddingLeft: 12, paddingRight: 12}}>
                                                <Text style={{fontSize: 16, fontWeight: '600', textAlign: 'center'}}>{this.state.tip}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ) : null
                        }
                    </View>
                    {
                        footer ? footer : (null)
                    }
                    {
                        message ?
                            <MessageBox visible={message.content} content={message.content} buttons={message.buttons} width={width} height={height}/> : null
                    }
                    {
                        dropdown ?
                            <SelectorModal options={dropdown.options} title={dropdown.title} width={width} height={height} selectedIndex={dropdown.selectedIndex} backdropClick={dropdown.backdropClick} onSelect={dropdown.onSelect}/> : null
                    }
                </View>
            </View>
        )
    }
}
