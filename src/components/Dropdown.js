import React from 'react';
import
{
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    TextInput,
    Image,
    Platform,
    StatusBar
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';

export default class Dropdown extends React.Component{
    render(){
        var {style, onClick, selectedValue, placeholder} = this.props;

        return (
            <View style={[style, {
                borderColor: '#7AB848',
                borderWidth: 2,
                borderRadius: 3,
            }]}>
                <TouchableOpacity onPress={onClick}>
                    <Text style={{
                        paddingTop: 10,
                        paddingBottom: 10,
                        paddingLeft: 12,
                        paddingRight: 30,
                        fontSize: config.font.default.fontSize,
                    }} ellipsizeMode={'tail'} numberOfLines={1}>{selectedValue ? selectedValue : placeholder ? placeholder : 'Please select...'}</Text>
                    <Image source={require('../images/caret.png')} style={{
                        height: 16,
                        width: 16,
                        position: 'absolute',
                        right: 15,
                        top: 16
                    }}/>
                </TouchableOpacity>
            </View>
        )
    }
}