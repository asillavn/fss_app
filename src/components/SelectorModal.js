/**
 * Created by NARA-ThanhTV on 8/18/2017.
 */
import React from 'react';
import
{
    View,
    ScrollView,
    TouchableOpacity,
    Text,
    TextInput,
    Image,
    Platform,
    StatusBar
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';

export default class SelectorModal extends React.Component{
    render(){
        var {options, width, height, title, selectedIndex, backdropClick, onSelect} = this.props;
        return (
            <TouchableOpacity style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: width,
                height: height,
                backgroundColor: 'rgba(0, 0, 0, .5)',
                zIndex: 98
            }} onPress={backdropClick}>
                <View style={theme.verticalContent}>
                    <View style={theme.horizontalContent}>
                        <View style={{
                            width: width * .8,
                        }}>
                            <View style={{
                                paddingTop: 20,
                                paddingBottom: 20,
                                backgroundColor: theme.main.backgroundColor,
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10
                            }}>
                                <Text style={{
                                    width: width * .8,
                                    textAlign: 'center',
                                    color: '#fff',
                                    fontSize: config.font.navButton.fontSize,
                                    fontWeight: '600',
                                }}>{title.toUpperCase()}</Text>
                            </View>
                            <View style={{
                                paddingBottom: 10,
                                borderBottomLeftRadius: 10,
                                borderBottomRightRadius: 10,
                                backgroundColor: '#fff',
                            }}>
                                <ScrollView style={{
                                    height: options.length * 70 > height - 140 ? height - 140 : options.length * 70,
                                }}>
                                    {
                                        options.map((item, idx)=>{
                                            return (
                                                <TouchableOpacity key={idx} style={{
                                                    borderBottomWidth: 2,
                                                    borderBottomColor: '#ddd',
                                                    backgroundColor: idx === selectedIndex ? '#ddd' : 'transparent'
                                                }} onPress={()=>{
                                                    onSelect(idx, item);
                                                }}>
                                                    <Text style={{
                                                        color: '#666',
                                                        paddingTop: 20,
                                                        paddingBottom: 20,
                                                        paddingLeft: 20,
                                                        paddingRight: 30,
                                                        fontSize: config.font.default.fontSize
                                                    }}>{
                                                        (item)
                                                    }</Text>
                                                    <Image source={require('../images/next.png')} style={{
                                                        height: 16,
                                                        width: 16,
                                                        position: 'absolute',
                                                        right: 20,
                                                        top: 27
                                                    }}/>
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}