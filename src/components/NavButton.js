import React from 'react';
import
{
    TouchableOpacity,
    Text,
    View
} from 'react-native';
import config from '../appConfig';
import theme from '../appTheme';

export default class NavButton extends React.Component{
    render(){
        let {onClick, title, textStyle, btnStyle} = this.props;
        //////console.log(this.props.children);
        return (
            <TouchableOpacity onPress={onClick} style={btnStyle}>
                <View>
                    {
                        this.props.children
                    }
                    <Text style={[textStyle, theme.navFont]}>{title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}