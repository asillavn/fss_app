import React from 'react';
import
{
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import config from '../appConfig';

export default class CaptureButton extends React.Component{
    render(){
        let {onClick, style} = this.props;
        let outerWidth = config.windowHeight / 7.5;
        let innerWidth = outerWidth - 20;

        return (
            <View style={[style, {
                alignItems: 'center',
                width: config.windowWidth,
                backgroundColor: 'transparent',
            }]}>
                <View style={{
                    height: outerWidth,
                    width: outerWidth,
                    borderRadius: outerWidth,
                    borderWidth: 7.5,
                    //borderColor: '#FFD55F',
                    borderColor: '#FFD55F',
                    backgroundColor: 'transparent',
                    flex: 1,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={onClick} style={{
                        width: innerWidth,
                        height: innerWidth,
                        borderRadius: innerWidth,
                        backgroundColor: '#FFF'
                    }}/>
                </View>
            </View>
        )
    }
}